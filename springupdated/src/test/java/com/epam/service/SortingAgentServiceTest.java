package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.service.SortingAgentService;
import com.epam.vo.ConsignmentVO;

@SpringBootTest
public class SortingAgentServiceTest
{
	@Autowired
	SortingAgentService sortingAgentService;
	
	@Test
	void checkIncomingConsignments()
	{
		List<ConsignmentVO> l = sortingAgentService.getListOfIncomingConsignments(2);
		assertNotNull(l);
	}
	
	
}
