package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.entity.Branch;

@SpringBootTest
public class AdminServiceTest 
{

	@Autowired
	AdminService adminService;
	
	@Test
	void branchNotEmpty()
	{
		List<Branch> branchList = adminService.getAllBranches();
		assertNotNull(branchList);
	}
}
