package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.entity.City;

@SpringBootTest
public class CityServiceTest {
	
	@Autowired
	CityService cityService;
	
	@Test
	void cityList()
	{
		List<City> cityList = cityService.getCities();
		assertNotNull(cityList);
	}

}
