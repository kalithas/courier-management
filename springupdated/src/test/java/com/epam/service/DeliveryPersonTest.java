package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.entity.DeliveryPerson;
import com.epam.repo.DeliveryPersonRepo;

@SpringBootTest
public class DeliveryPersonTest {

	@Autowired
	DeliveryPersonRepo deliveryPersonRepo;

	@Test
	void ifExists() {
		DeliveryPerson deliveryPerson = deliveryPersonRepo.findById(1);
		assertNotNull(deliveryPerson);
	}
}