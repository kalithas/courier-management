package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.entity.Customer;
import com.epam.repo.CustomerRepo;

@SpringBootTest
public class CustomerServiceTest {
	@Autowired
	private CustomerRepo customerRepo;

	@Test
	void getAll() {
		Customer customer = customerRepo.findByUserId(1);
		assertNull(customer);
	}
}