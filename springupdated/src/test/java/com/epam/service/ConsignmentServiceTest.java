package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.service.ConsignmentService;
import com.epam.vo.ConsignmentVO;

@SpringBootTest
public class ConsignmentServiceTest {

	@Autowired
	ConsignmentService consignmentService;
	
	@Test
	void trackTest()
	{
		ConsignmentVO cvo = consignmentService.trackConsignment("abc");
		System.out.println(consignmentService);
		assertNotNull(cvo);
	}
}
