package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.entity.Branch;
import com.epam.repo.BranchRepo;
import com.epam.service.impl.AdminServiceImpl;

@SpringBootTest
public class BranchServiceTest {

	@Autowired
	private BranchRepo branchRepo;

	private AdminService adminService;

	@BeforeEach
	void setUp() {
		this.adminService = new AdminServiceImpl(this.branchRepo);
	}

	@Test
	void getAllBranches() {

		List<Branch> reBranchs = branchRepo.findAll();
		assertNotNull(reBranchs);
	}
}