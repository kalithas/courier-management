package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.util.CourierUtil;

@SpringBootTest
public class CourierUtilTest {

	@Test
	void passwordTest()
	{
		String password = CourierUtil.getPassword();
		assertEquals(15, password.length());
	}
	
	@Test
	void otpTest()
	{
		String otp = CourierUtil.sendOtp(1);
		boolean otpVerification = CourierUtil.verifyOtp(1, otp);
		assertNotNull(otpVerification);
	}
}
