<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
	
    <style>
      @import url("https://fonts.googleapis.com/css2?family=Raleway:ital,wght@1,200&display=swap");

      * {
        margin: 0;
        padding: 0;
        border: 0;
      }

      body {
        font-family: "Raleway", sans-serif;
        background-color: #d8dada;
        font-size: 19px;
        max-width: 800px;
        margin: 0 auto;
        padding: 3%;
      }

      img {
        max-width: 100%;
      }

      header {
        width: 98%;
      }

      #logo {
        max-width: 120px;
        margin: 3% 0 3% 3%;
        float: left;
      }

      #wrapper {
        background-color: #f0f6fb;
      }

      #social {
        float: right;
        margin: 3% 2% 4% 3%;
        list-style-type: none;
      }

      #social > li {
        display: inline;
      }

      #social > li > a > img {
        max-width: 35px;
      }

      h1,
      p {
        margin: 3%;
      }
      .btn {
        float: right;
        margin: 0 2% 4% 0;
        background-color: #303840;
        color: #f6faff;
        text-decoration: none;
        font-weight: 800;
        padding: 8px 12px;
        border-radius: 8px;
        letter-spacing: 2px;
      }

      hr {
        height: 1px;
        background-color: #303840;
        clear: both;
        width: 96%;
        margin: auto;
      }

      #contact {
        text-align: center;
        padding-bottom: 3%;
        line-height: 16px;
        font-size: 12px;
        color: #303840;
      }
    </style>
	
	<style type="text/css">
.bgimg {
    background-image: url('https://raw.githubusercontent.com/Sagar-T/courier/main/hiclipart.co.png');
}
</style>
  </head>
<body >
	<table width="60%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1d1d1d">
		<tr>
			<td align="center valign="top">
				<!-- Header -->
				
				<!-- Logo + Socials Icons -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" style="padding: 10px 0px;" class="p30-15">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<th class="column" width="140" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<!-- Logo -->
															<td class="img m-center" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://raw.githubusercontent.com/Sagar-T/courier/main/logo3.png" width="172" height="37" editable="true" border="0" alt="" /></td>
															<!-- END Logo -->
														</tr>
													</table>
												</th>
												
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- Logo + Socials Icons -->
				<!-- END Header -->

				<repeater>
					<!-- Intro -->
					<layout label='Intro'>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<table width="300" border="1" cellspacing="0" cellpadding="0" class="mobile-shell">
										<tr>
											<td class="td" style="width:100px; min-width:80px; font-size:0pt; line-height:0pt; padding:50; margin:40; font-weight:normal;">
												<table width="70%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td background="images/free_bg.jpg" bgcolor="#a3854b" valign="top" height="40" class="bg">
														
															<div class="bgimg">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="content-spacing" width="30" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																		<td style="padding: 40px 0px;" class="p30-0">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="box1" bgcolor="#deaf84" align="center" style="padding:45px 30px;">
																						<table border="0" cellspacing="0" cellpadding="0">
																							
																							<tr>
																								<td class="h2 center pb20" style="color:#000000; font-family:'Playfair Display', Georgia, serif; font-size:50px; line-height:54px; font-weight:bold; text-transform:uppercase; text-align:center; padding-bottom:20px;"><multiline>ProDeliver!</multiline></td>
																							</tr>
																							<tr>
																								<td class="text4 center pb30" style="color:#E71730; font-family:'Raleway', Arial,sans-serif;  font-weight: bold; font-size:25px; line-height:5px; text-align:center; padding-bottom:30px;"><multiline>Dear ${Name},</multiline></td></br>
																							</tr>
																							<tr>
																								<td class="text4 center pb30" style="color:#E71730; font-family:'Raleway', Arial,sans-serif;  font-weight: bold; font-size:14px; line-height:18px; text-align:center; padding-bottom:30px;"><multiline> Thanks for trusting us with your package. Your order has been placed. Your tracking id is ${consignmentId}</multiline></td></br>
																							</tr>
																							<tr>
																								<td class="text4 center pb30" style="color:#E71730; font-family:'Raleway', Arial,sans-serif;  font-weight: bold; font-size:14px; line-height:18px; text-align:center; padding-bottom:30px;"><multiline>Track your package status with just <b> <strong>one click.</b></strong></multiline></td>
																							</tr>
																							<!-- Button -->
																							<tr>
																								<td align="center">
																									<table width="140" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="text-button black-button" style="font-family:'Raleway', Arial, sans-serif; font-size:12px; line-height:16px; text-align:center; text-transform:uppercase; border:2px solid #ffffff; padding:12px 20px; background:#000000; color:#ffffff;"><multiline><a href="http://localhost:8089/" target="_blank" class="link-white" style="color:#ffffff; text-decoration:none;"><span class="link-white" style="color:#ffffff; text-decoration:none;">Track package</span></a></multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<!-- END Button -->
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	
																	</tr>
																</table>
															</div>

														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</layout>
					<!-- END Intro -->

					<!-- Offers -->
					<layout label='Offers'>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" style="padding: 1px 0px 10px 0px;" class="p30-15">
									<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
										<tr>
											<td class="td" style="width:250px; min-width:150px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<th  width="30" style="font-size:0pt; line-height:0pt; padding:100px; margin:0; font-weight:normal;"></th>
																	<th  width="130" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0px">
																			<tr>
																				<td  style="font-size:0pt; line-height:0pt; padding-top:5px; "><img src="https://raw.githubusercontent.com/Sagar-T/courier/main/delivery_boy.png" width="210" height="309" editable="true" border="0" alt="" /></td>
																			</tr>
																			<tr>
																				<td class="h4 center pb10" style="color:#ffffff; font-family:'Playfair Display', Georgia, serif; font-size:17px; line-height:20px; text-transform:uppercase; text-align:center; padding-bottom:5px;"><multiline>Delivery to your doorstep</multiline></td>
																			</tr>
																			<tr>
																				<td  style="color:#979797; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:24px; text-align:center; padding-bottom:5px;"><multiline>It's not fast, its superfast delivery</multiline></td>
																			</tr>
																		
																		
																		</table>
																	</th>
																	
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>	
								</td>
							</tr>
						</table>
					</layout>
		

				
					</layout>
					<!-- END Product / Image On The Right -->

					<!-- Contacts -->
					<layout label='Contacts'>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" class="p30-15" style="padding: 0px 0px 10px 0px;">
									<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
										<tr>
											<td class="td" style="width:70px; min-width:50px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<th class="column" width="110" height="10" bgcolor="#1d1d1d" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="box" style="padding:15px 10px; border:1px solid #5a5a5a;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="img" width="75" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPpDydWfyKavK3bnOeGZGL1q6KWy778MN5Cg&usqp=CAU" width="24" height="28" editable="true" border="0" alt="" /></td>
																				<td class="h5-4 left" style="color:#ffffff; font-family:'Playfair Display', Georgia, serif; font-size:10px; line-height:18px; text-transform:uppercase; text-align:left;">
																					<multiline>
																						<strong><a href="#" target="_blank" class="link" style="color:#ffffff; text-decoration:none; ;"><span class="link" style="color:#ffffff; text-decoration:none;"\>Email</span></a></strong>
																						<br />
																						<span  style="color:#d1a24c;">@donotreplytoprodelivery@gmail.com</</span>
																					</multiline>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</th>
														<th class="column-empty" width="30" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
														<th class="column" width="100" bgcolor="#1d1d1d" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="box" style="padding:15px 10px; border:1px solid #5a5a5a;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="img" width="75" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://raw.githubusercontent.com/Sagar-T/courier/main/t6_ico2.png" width="34" height="28" editable="true" border="0" alt="" /></td>
																				<td class="h5-4 left" style="color:#ffffff; font-family:'Playfair Display', Georgia, serif; font-size:13px; line-height:18px; text-transform:uppercase; text-align:left;">
																					<multiline>
																						<strong><a href="#" target="_blank" class="link" style="color:#ffffff; text-decoration:none;"><span class="link" style="color:#ffffff; text-decoration:none;"\>Call us</span></strong>
																						<br />
																						<span  style="color:#d1a24c;">9851001589</</span></a>
																					</multiline>
																				</td>
																				<td class="h5-4 left" style="color:#ffffff; font-family:'Playfair Display', Georgia, serif; font-size:13px; line-height:18px; text-transform:uppercase; text-align:left;">
																				
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
												</table>
											</td>
										</tr>
									</table>	
								</td>
							</tr>
						</table>
					</layout>
				
				</repeater>

			
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1d1d1d">
					<tr>
						<td class="text-footer p30-15" style="padding: 28px 10px 50px; color:#88A8B5; font-family:'Raleway', Arial,sans-serif; font-size:12px; line-height:20px; text-align:center;">
						<multiline>ProDelivery is happy to be your first choice.</multiline>	<br />
						<span  style="color:#d1a24c;">©ProDeliver2022,</</span>
						<span  style="color:#d1a24c;"></br>${Location}</</span>
																				
						</td>
						
						</tr>
				</table>
			
			</td>
		</tr>
	</table>
</body>
</html>
