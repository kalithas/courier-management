<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ProDelivery</title>

<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">

<!-- Stylesheets -->

<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="//code.jquery.com/jquery-1.10.2.js">
	
</script>

<script>
	$(function() {
		$("#header").load("../Header.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
</head>
<body class="boxed_wrapper">
	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="row">
		<div class="container">
			<h2>All Available Branches in ${city}</h2>
			<div class="list-group">
				<c:forEach var="b" items="${branch}">
				<div class="list-group">
					<a href="#" class="list-group-item active">
						<h4 class="list-group-item-heading">${b.city}</h4>
						<p class="list-group-item-text">${b.address}</p>
						<p class="list-group-item-text">${b.zipCode}</p>
						<br>
					</a>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>


	<div id="footer"></div>

	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>

	<style>
.card-custom {
	max-width: 128px;
}
</style>
	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>
</body>
</html>