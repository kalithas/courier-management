<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">





<title>Add Consignment</title>





<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">





<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../BranchEmployeePageHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
<script>
	function success() {
		alert("Added Successfully");
		return true;
	}
</script>
</head>
<body class="boxed_wrapper">





	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->





	<div id="header"></div>
	<br>





	<section class="contact-section">
		<div class="container">
			<div class="image-container">
				<figure class="image-box">
					<img src="images/resource/courier.png" alt="">
				</figure>
			</div>
			<div class="contact-form-area">
				<div class="sec-title center">
					<h2>Add Consignment</h2>
				</div>
				<div class="form-inner">
					<form method="post" id="contact-form onsubmit = " return
						success()"
class="default-form">
						<div class="row">
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-user"></i> <input type="text" name="name"
										placeholder="Name" required>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-envelope"></i> <input type="email"
										name="email" placeholder="Email" required>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-home"></i> <input type="text" name="source"
										placeholder="Source" required>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-home"></i> <input type="text"
										name="destination" placeholder="destination" required>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<textarea name="sourceaddress" placeholder="Source Address"></textarea>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<textarea name="destinationaddress"
										placeholder="Destination Address"></textarea>
								</div>
							</div>



							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-map-pin"></i> <input type="text"
										name="sourcepin" placeholder="Source Pincode" required>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-map-pin"></i> <input type="text"
										name="destinationpin" placeholder="Destination Pincode"
										required>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<select name="modeofdelivery">
										<optgroup label="Mode of Delivery">
											<option value="Airways">Airways</option>
											<option value="Landways">Landways</option>
										</optgroup>
									</select>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-weight-hanging"></i> <input type="number"
										step="any" name="weight" placeholder="weight" required>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 column">
								<div class="form-group message-btn centred">
									<button type="submit" class="theme-btn-two" name="submit-form">Submit
										Now</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>





	<div id="footer"></div>





	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>






	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>





	<!-- main-js -->
	<script src="../js/script.js"></script>
</body>
</html>