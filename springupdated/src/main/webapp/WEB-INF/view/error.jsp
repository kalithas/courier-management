<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Error Page</title>

<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>

<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Noto+Sans:700');
@import url('https://fonts.googleapis.com/css?family=Lato');
body
{
	margin:0;
	padding:0;
	padding:5%;
  font-family: 'Lato', sans-serif;
}
.err_page
{
	width:100%;
  height:80%;
	margin:4% auto;
	background:#fff;
	text-align:center;
	display:flex;
	align-items: center;
}
.err_page_right
{
	width:100%;
}
.err_page_left
{
		width:100%;
}
.err_page h1
{
	font-family: 'Noto Sans', sans-serif;
	font-size:70pt;
	margin:0;
	color:#6400ff;
}
.err_page h4
{	color:#6400ff;
	font-size:14pt;
}
.err_page p
{
	font-size:14pt;
	color: #737373;
}
.err_btn
{
	background:#fff;
	border:2px solid #6400ff;
	padding:20px;
	border-radius:5px;
	box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
	cursor:pointer;
	font-size:13pt;
	transition:background 0.5s;
}
.err_btn:hover
{
	background:#6400ff;
  box-shadow: 0px 8px 10px rgba(0, 0, 0, 0.2);
	color:#fff;
}
.credit
{
  position:absolute;
  bottom:0;
  right:0;
}
@media screen and (max-width: 800px)
{
  .err_page
  {
     flex-direction:column;  
  }
  .err_page_left img
  {
       width:250px;
      height:auto;
  }
  .err_page h4
  {
    margin:0;
  }
}
/*-----------NOT NEEDED---------*/
@media screen and (max-width: 350px)
{
  .credit
  {
    visibility:hidden;
  }
}
</style>

</head>
<body>
	<div class="err_page">
		<div class="err_page_left">
			<img src="../../images/resource/error.png" width=360px height=250px/>
		</div>
		<div class="err_page_right">
			<h1>404</h1>
			<h4>OOPS! Something went wrong </h4>
			<p>Don't worry. Please Try Again....</p>
			<button class="err_btn" onclick="history.back()">GO Back</button>
		</div>	
	
	</div>
</body>
</html>