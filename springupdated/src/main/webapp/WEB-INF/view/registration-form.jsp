<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Registration Form</title>
</head>
<body>
    <div align="center">
        <h2>User Registration</h2>
        <form:form action="register" method="post" modelAttribute="reservation">
            
             
            <form:label path="email">E-mail:</form:label>
            <form:input path="email"/><br/>
             
            <form:label path="password">Password:</form:label>
            <form:password path="password"/><br/>      
 
            
                 
            <form:button>Register</form:button>
        </form:form>
    </div>
</body>
</html>