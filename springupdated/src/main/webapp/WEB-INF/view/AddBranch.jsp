<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ProDelivery</title>

<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../AdminPageHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
</head>
<body class="boxed_wrapper">
	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>
	<br><br><br><br><br><br><br><br>
	<div class="row">
					<div class="col-sm-4 offset-sm-4">
					
						<form action = "addBranch" method = "POST">
							<div class='form-group'>
								<label for="basicSelect1">Select City</label> <select
									name="city" onchange='CheckColors(this.value);'>
									<option></option>
										<c:forEach var="c" items="${city}">

											<option>${c.cityName}</option>

										</c:forEach>
 
									<option value="others">others</option></select>
								<input class="form-control" type="text" name="city" id="color"
									style='display: none;' />
									
							</div>
							<div class="form-group">
								<label for="address">Address</label> <input
									class="form-control" type="text" name = "address" id="address"
									placeholder="Address">
							</div>
							<div class="form-group">
								<label for="zipcode">Zip Code</label> <input
									class="form-control" type="text" id="zipcode" name = "zipCode"
									placeholder="Zip Code">
							</div>

							<div class="form-group">
								<label for="basicEmailInput">Email Address</label> <input
									class="form-control" type="email" id="basicEmailInput" name = "mail"
									placeholder="Email">
							</div>
							
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
	
	<div id="footer"></div>

<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
	<span class="fa fa-arrow-up"></span>
</button>


<!-- jequery plugins -->
<script src="../js/jquery.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/owl.js"></script>
<script src="../js/wow.js"></script>
<script src="../js/validation.js"></script>
<script src="../js/jquery.fancybox.js"></script>
<script src="../js/appear.js"></script>
<script src="../js/circle-progress.js"></script>
<script src="../js/jquery.countTo.js"></script>
<script src="../js/scrollbar.js"></script>
<script src="../js/jquery.paroller.min.js"></script>
<script src="../js/tilt.jquery.js"></script>

<!-- main-js -->
<script src="../js/script.js"></script>

<script type="text/javascript">
	function CheckColors(val) {
		var element = document.getElementById('color');
		if (val == 'Select City' || val == 'others')
			element.style.display = 'block';
		else
			element.style.display = 'none';
	}
</script>

</body>
</html>