<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">





<title>Add Consignment</title>





<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">





<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../BranchEmployeePageHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
<script>
	function success() {
		alert("Added Successfully");
		return true;
	}
</script>
</head>
<body class="boxed_wrapper">





	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->





	<div id="header"></div>
	<br>



	<section class="contact-section">
		<div class="container">
			<div class="image-container">
				<figure class="image-box">
					<img src="images/resource/courier.png" alt="">
				</figure>
			</div>
			<div class="contact-form-area">
				<div class="sec-title center">
					<h2>Invoice of Your Consignment</h2>
				</div>
				<div class="container-fluid"
					style="background-color: #FFF3AA; color: #000;">
					<div class="row">
						<div class="col-md-12 col-md-offset-3 body-main">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-8 text-right">
										<h4 style="color: #F81D2D;">
											<strong>ProDelivery</strong>
										</h4>
										<p>${source}</p>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-md-12 text-center">
										<h2>INVOICE</h2>
									</div>
								</div>
								<br />
								<div>
									<table class="table">
										<thead>
											<tr>
												<th>
													<h5>Description</h5>
												</th>
												<th>
													<h5>Weight</h5>
												</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="col-md-9">Package</td>
												<td class="col-md-3"><i class="fas fa-weight-hanging"
													area-hidden="true"></i> ${weight}</td>
											</tr>
											<tr>
												<td class="text-right">
													<p>
														<strong>Total Amount: </strong>
													</p>
												</td>
												<td>
													<p>
														<strong><i class="fas fa-rupee-sign"
															area-hidden="true"></i>${price} </strong>
													</p>
												</td>
											</tr>
											<tr style="color: #F81D2D;">
												<td class="text-right">
													<h4>
														<strong>Total Payable Amount:</strong>
													</h4>
												</td>
												<td class="text-left">
													<h4>
														<strong><i class="fas fa-rupee-sign"
															area-hidden="true"></i> ${price} </strong>
													</h4>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button class="btn btn-primary"
					onclick="location.href='/BC/createConsignment'">Add
					Another Consignment</button>



			</div>
		</div>
	</section>




	<div id="footer"></div>





	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>






	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>





	<!-- main-js -->
	<script src="../js/script.js"></script>
</body>
</html>