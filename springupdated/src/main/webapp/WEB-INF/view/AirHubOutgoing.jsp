<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">
<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>

<script>
	$(function() {
		$("#header").load("../AirHubHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
<style type="text/css">
table {
	width: 750px;
	border-collapse: collapse;
	margin: 50px auto;
}

/* Zebra striping */
tr:nth-of-type(odd) {
	background: #fff;
	color: #4527a4;
}

th {
	background: #4527a4;
	color: white;
	font-weight: bold;
}

td, th {
	padding: 10px;
	border: 1px solid #ccc;
	text-align: left;
	font-size: 18px;
}

/*
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media only screen and (max-width: 760px) , ( min-device-width : 768px)
	and (max-device-width: 1024px) {
	table {
		width: 100%;
	}

	/* Force table to not be like tables anymore */
	table, thead, tbody, th, td, tr {
		display: block;
	}

	/* Hide table headers (but not display: none;, for accessibility) */
	thead tr {
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
	tr {
		border: 1px solid #ccc;
	}
	td {
		/* Behave like a "row" */
		border: none;
		border-bottom: 1px solid #eee;
		position: relative;
		padding-left: 50%;
	}
	td:before {
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%;
		padding-right: 10px;
		white-space: nowrap;
		/* Label the data */
		content: attr(data-column);
		color: #000;
		font-weight: bold;
	}
}
</style>
</head>
<body class="boxed_wrapper">

	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<div class="row">
		<div class="container">
<!-- 		<form action = "/AHC/receiveOutgoingconsignmnet" method = "POST"> -->
			<tbody>
			
				<h2>List Of Outgoing Shipments</h2>
				<div class="list-group">

					<%!int sNo = 1;%>

					<table>
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Source</th>
								<th>Destination</th>
								<th>Tracking ID</th>
								<th>Mark</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="r" items="${conVO}">
								<tr>
									<td>
										<%
										out.println(sNo++);
										%> <th>${r.source}</th>
									<th>${r.destination}</th>
									<th>${r.trackingId}</th>
								</td>
								<td><button
									onclick="location.href='/AHC/outgoing/${r.trackingId}'"
									class="btn btn-success">Dispatch</button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
			</div>
			</tbody>
			
<!-- 			</form> -->
		</div>

	</div>

	<div id="footer"></div>




	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>


	<!-- jequery plugins -->
	<script src="../js/jquery.js">
		
	</script> <script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>


									</body>
</html>