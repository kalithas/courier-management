<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">







<title>Edit Profile</title>







<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">







<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../AirHubHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
<script>
			
			function validatePassword() {
				  var x = document.getElementById("oldPassword").value;
				  var y = document.getElementById("newPassword").value;
				  if (x != y) {
				    alert("Re-Enter your password");
				    return false;
				  }
				  
				}
			
</script>
</head>
<body class="boxed_wrapper">



	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->



	<div id="header"></div>
	<br>




	<section class="contact-section">
		<div class="container">
			<div class="image-container">
				<figure class="image-box">
					<img src="images/resource/courier.png" alt="">
				</figure>
			</div>
			<div class="contact-form-area">

				<div class="form-inner">




					<form method="post" action="#" id="contact-form" onsubmit = "return validatePassword()"
						class="default-form">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-email"></i> <input type="text" name="email"
										placeholder="Email" >
								</div>
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-password"></i> <input type="password"
										name="oldPassword" placeholder="Old Password">
								</div>
							</div>



							<div class="col-lg-6 col-md-12 col-sm-12 column">
								<div class="form-group">
									<i class="fas fa-password"></i> <input type="password"
										name="newPassword" placeholder="New Password">
								</div>
							</div>



							<div class="col-lg-12 col-md-12 col-sm-12 column">
								<div class="form-group message-btn centred">
									<button type="submit" class="theme-btn-two" name="submit-form">Update</button>
								</div>
							</div>
						</div>
					</form>



				</div>
			</div>
		</div>
	</section>







	<div id="footer"></div>







	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>








	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>







	<!-- main-js -->
	<script src="../js/script.js"></script>
</body>
</html>