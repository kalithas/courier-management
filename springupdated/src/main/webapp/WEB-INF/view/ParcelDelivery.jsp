<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">

<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="..//code.jquery.com/jquery-1.10.2.js">
	
</script>

<script>
	$(function() {
		$("#header").load("../Header.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
</head>
<body class="boxed_wrapper">

	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>
	<br>
	<!-- our-history -->
	<section class="our-history">
		<div class="container">
			<div class="image-container">
				<figure class="image-box" style="height: 20%"></figure>
				<img src="../images/background/parcel_world.jpg" alt="">
				</figure>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 content-column">
					<div id="content_block_53">
						<div class="content-box wow fadeInLeft" data-wow-delay="00ms"
							data-wow-duration="1500ms">
							<div class="sec-title">
								<h2>What we do</h2>
							</div>
							<div class="text">
								<p>To make your life as easy as possible, we offer you
									several tracking options. You can track your consignment
									on-line or via e-mail, which ever option is convenient for you.
									ProDeliver is Web based shipment tracking Solution which is
									generally suitable for low volume and non frequent customer. No
									software installation is required to track the shipment. Any
									system connected with Internet will be enough to track the
									shipment. Customer can track the shipment using ProDeliver
									Consignment No/Tracking Id or corresponding Reference No.</p>



								<h4><b>Here's how you can avail our service:</b></h4>
									<p>�You can order anywhere through out India in our nearby
										 ProDeliver/ Any courier service center.</br>
									�Our tracking service provides you the most enhanced tracking system experience where you can see the current
										  status of your package.</br>
									�Track you package via email service.</br>
									�Get all our package updates via email.</p>
								<h4>Hurray !! you are all set to place an order with us...</h4>
								<p><b>Happy to serve you !!</b></p>




					
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 image-column">
					<div id="image_block_47">
						<div class="image-box js-tilt">
							<figure class="image wow slideInRight" data-wow-delay="00ms"
								data-wow-duration="1500ms">
								<img src="images/resource/illustration-41.png" alt="">
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- our-history end -->

	<div id="footer"></div>

	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>

	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>

</body>
</html>