<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>ProDelivery</title>

<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">

<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="..//code.jquery.com/jquery-1.10.2.js">
	
</script>

<script>
	$(function() {
		$("#header").load("../Header.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>

</head>
<body class="boxed_wrapper">

	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>
	
	

    <!-- banner-section -->
    <section class="banner-style-ten">
        <div class="image-layer" style="background-image: url(../images/icons/banner-bg-9.png);"></div>
        
    </section>
    <!-- banner-section end -->
	
	<!-- service-style-three -->
	<section class="service-style-three">
		<div class="image-layer wow slideInLeft" data-wow-delay="00ms"
			data-wow-duration="1500ms"
			style="background-image: url(../images/icons/shap-12.png);"></div>
		<div class="container">
			<div class="inner-container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-12 single-column">
						<div class="sec-title">
							<h2>Our Services</h2>
						</div>
						<div class="service-block-three wow fadeInUp"
							data-wow-delay="300ms" data-wow-duration="1500ms">
							<div class="inner-box">
								<div class="icon-box">
									<div class="bg-layer"
										style="background-image: url(../images/icons/icon-bg-3.png);"></div>
									<i class="flaticon-profit-1"></i>
								</div>
								<h3>
									<a href="/OC/parcelDelivery">Parcel Delivery</a>
								</h3>
								<div class="text"></div>
								<div class="link-btn">
									<a href="/OC/parcelDelivery"><i class="far fa-arrow-alt-circle-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 single-column">
						<div class="service-block-three wow fadeInUp"
							data-wow-delay="300ms" data-wow-duration="1500ms">
							<div class="inner-box">
								<div class="icon-box">
									<div class="bg-layer"
										style="background-image: url(../images/icons/icon-bg-1.png);"></div>
									<i class="flaticon-mail"></i>
								</div>
								<h3>
									<a href="/">Shipment Tracking</a>
								</h3>
								<div class="text"></div>
								<div class="link-btn">
									<a href="/"><i class="far fa-arrow-alt-circle-right"></i></a>
								</div>
							</div>
						
					</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 single-column">
						<div class="service-block-three wow fadeInUp"
							data-wow-delay="900ms" data-wow-duration="1500ms">
							
						</div>
						<div class="service-block-three wow fadeInUp"
							data-wow-delay="300ms" data-wow-duration="1500ms">
							<div class="inner-box">
								<div class="icon-box">
									<div class="bg-layer"
										style="background-image: url(../images/icons/icon-bg-5.png);"></div>
									<i class="flaticon-server"></i>
								</div>
								<h3>
									<a href="/OC/branch">Check Service Availability</a>
								</h3>
								<div class="text"></div>
								<div class="link-btn">
									<a href="/OC/branch"><i class="far fa-arrow-alt-circle-right"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- service-style-three end -->

	<div id="footer"></div>




	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>


	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>
</body>
</html>