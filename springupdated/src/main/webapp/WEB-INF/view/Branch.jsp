<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="..//code.jquery.com/jquery-1.10.2.js"></script>

<script>
	$(function(){
		$("#header").load("../Header.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
</head>
<body class="boxed_wrapper">

	<!-- preloader -->
    <div class="preloader"></div>
    <!-- preloader -->

    <div id="header"></div>

	
    <!-- banner-section -->
    <section class="banner-style-18">
        <div class="anim-icons">
            <div class="icon icon-1"><img src="../images/icons/anim-icon-15.png" alt=""></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div class="content-box">
                        <h1>Anytime, Anywhere across India</h1>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                    <div class="image-box js-tilt">
                        <figure class="image"><img src="../images/resource/search-branchs.png" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

	<!-- domain-section -->
    <section class="domain-section">
        <div class="container">
            <div class="inner-container">
                <div class="sec-title center"><h2>Looking For nearest branch?</h2></div>
                <div class="search-form">
                    <form method="post">
                        <div class="form-group">
                            <input type="text" name="city" placeholder="Enter Your City" required="">
                            <button type="submit">Search Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- domain-section end -->
	
	 <div id="footer"></div>
    
    <!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-arrow-up"></span>
</button>
    
    <!-- jequery plugins -->
<script src="../js/jquery.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/owl.js"></script>
<script src="../js/wow.js"></script>
<script src="../js/validation.js"></script>
<script src="../js/jquery.fancybox.js"></script>
<script src="../js/appear.js"></script>
<script src="../js/circle-progress.js"></script>
<script src="../js/jquery.countTo.js"></script>
<script src="../js/scrollbar.js"></script>
<script src="../js/jquery.paroller.min.js"></script>
<script src="../js/tilt.jquery.js"></script>

<!-- main-js -->
<script src="../js/script.js"></script>
	
</body>
</html>