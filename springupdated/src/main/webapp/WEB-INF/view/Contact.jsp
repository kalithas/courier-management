<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>Appway - HTML 5 Template Preview</title>

<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="..//code.jquery.com/jquery-1.10.2.js">
</script>

<script>
	$(function(){
		$("#header").load("../Header.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
</head>
<body class="boxed_wrapper">

    <!-- preloader -->
    <div class="preloader"></div>
    <!-- preloader -->

    <div id="header"></div>
	<br>

    <!-- contact-section -->
    <section class="contact-section">
        <div class="container">
         <div class="image-container">
                        <figure class="image-box"><img src="../images/resource/courier.png" alt=""></figure>
                    </div>
            <div class="row">
                <div class="col-lg-10 col-md-12 col-sm-12 offset-lg-1 big-column">
                    <div class="info-content centred">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                                <div class="single-info-box">
                                    <figure class="icon-box"><img src="../images/icons/info-icon-1.png" alt=""></figure>
                                    <h2>Phone</h2>
                                    <div class="text">Start working ProDelivery that can provide every services related to parcel</div>
                                    <div class="phone"><a href="tel:0665184575181">+91 8709840503</a></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                                <div class="single-info-box">
                                    <figure class="icon-box"><img src="../images/icons/info-icon-2.png" alt=""></figure>
                                    <h2>E-mail</h2>
                                    <div class="phone"><a href="#">prodelivery@info.com</a></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                                <div class="single-info-box">
                                    <figure class="icon-box"><img src="../images/icons/info-icon-3.png" alt=""></figure>
                                    <h2>Address</h2>
                                    <div class="text">Salarpuria, Hyderabad, India</div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="contact-form-area">
                        <div class="sec-title center"><h2>Contact us</h2></div>
                        <div class="form-inner">
                            <form method="post" action="sendemail.php" id="contact-form" class="default-form"> 
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <i class="fas fa-user"></i>
                                            <input type="text" name="username" placeholder="Name" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <i class="fas fa-envelope"></i>
                                            <input type="email" name="email" placeholder="Email" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <i class="fas fa-file"></i>
                                            <input type="text" name="subject" placeholder="Subject" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <i class="fas fa-phone"></i>
                                            <input type="text" name="phone" placeholder="Phone" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <textarea name="message" placeholder="Write here message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 column">
                                        <div class="form-group message-btn centred">
                                            <button type="submit" class="theme-btn-two" name="submit-form">Submit Now</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact-section end -->


    
    <div id="footer"></div>
    
    <!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-arrow-up"></span>
</button>


<!-- jequery plugins -->
<script src="../js/jquery.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/owl.js"></script>
<script src="../js/wow.js"></script>
<script src="../js/validation.js"></script>
<script src="../js/jquery.fancybox.js"></script>
<script src="../js/appear.js"></script>
<script src="../js/circle-progress.js"></script>
<script src="../js/jquery.countTo.js"></script>
<script src="../js/scrollbar.js"></script>
<script src="../js/jquery.paroller.min.js"></script>
<script src="../js/tilt.jquery.js"></script>

<!-- main-js -->
<script src="../js/script.js"></script>
</body>
</html>