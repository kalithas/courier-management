<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Success</title>
<style type="text/css">
    span {
        display: inline-block;
        width: 200px;
        text-align: left;
    }
</style>
</head>
<body>
    <div align="center">
        <h2>Registration Succeeded!</h2>
        
        <span>E-mail:</span><span>${reservation.email}</span><br/>
        <span>Password:</span><span>${reservation.password}</span><br/>
        <span>First Name :</span><span>${reservation1.firstName}</span><br/>
        <span>Last Name : </span><span>${reservation1.lastName}</span>
        
    </div>
</body>
</html>