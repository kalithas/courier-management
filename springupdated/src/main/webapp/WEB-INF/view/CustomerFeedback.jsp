<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Feedback Page</title>



<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">



<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">



<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../CustomerPageHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>

</head>
<body class="boxed_wrapper">
	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->



	<div id="header"></div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="row">
		<div class="col-sm-4 offset-sm-4">



			<form method="post" id="contact-form" class="default-form"
				onsubmit="return success()">
				<div class='form-group'>
					<label for="basicSelect1">Select Consignment</label><br> <select
						class="form-control" name="trackingId"
						onchange='CheckColors(this.value);'>



						<c:forEach var="c" items="${consignments}">



							<option>${c.trackingId}</option>



						</c:forEach>



					</select>
					<!-- <input class="form-control" type="text" name="consignmentId"
id="color" style='display: none;' /> -->



				</div>
				<div class="form-group">
					<label for="address">Email</label> <input class="form-control"
						type="text" name="email" id="email" value='${userEmail}' readonly>
				</div>
				<div class="form-group">
					<label for="address">Feedback</label>
					<!-- <textarea name="description"></textarea> -->
					<input class="form-control" type="text" name="description"
						id="description">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>



	<div id="footer"></div>



	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>




	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>



	<!-- main-js -->
	<script src="../js/script.js"></script>




</body>
</html>