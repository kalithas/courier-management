<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet">

<style>
body {
	background: #94b5c0;
}

.table {
	background: #ee99a0;
	border-radius: 0.2rem;
	width: 100%;
	padding-bottom: 1rem;
	color: #212529;
	margin-bottom: 0;
}

.table th:first-child, .table
td:first-child {
	position: sticky;
	left: 0;
	background-color: #ad6c80;
	color: #373737;
}

.table td {
	white-space: nowrap;
}
</style>
</head>
<body>
	<div class="container">
		<div class="row mt-3">
			<div class="col-3"></div>
			<div class="col-6">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col">Employee Name</th>
								<th scope="col">ID</th>
								<th scope="col">email</th>
								<th scope="col">password</th>
								<th scope="col">Type</th>
								<th scope="col">DOB</th>
								<th scope="col">Phone</th>
								<th scope="col">Aadhar No.</th>
								<th scope="col">Address</th>
								<th scope="col">city</th>
								<th scope="col">state</th>

							</tr>
						</thead>
						<tbody>
							<tr>
								<th>Sagar Thapa</th>
								<td>Sag1001Pro</td>
								<td>Sagar_Thapa@prodelivery.com</td>
								<td>Sagar@123</td>
								<td>Branch Employee</td>
								<td>21-01-1998</td>
								<td>9900117711</td>
								<td>974244641099</td>
								<td>Darjeeling</td>
								<td>Darjeeling</td>
								<td>West Bengal</td>
							</tr>
							<tr>
								<th>Ajay Shyam</th>
								<td>Aja1002Pro</td>
								<td>Ajay_Shyam@prodelivery.com</td>
								<td>Ajay@123</td>
								<td>Sorting Agent</td>
								<td>11-04-1983</td>
								<td>8798760711</td>
								<td>644244641094</td>
								<td>Dhanbad</td>
								<td>Dhanbad</td>
								<td>Jharkhand</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-3"></div>
		</div>
	</div>
</body>
</html>