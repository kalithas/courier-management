<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">



<title>View Profile</title>



<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">



<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../CustomerPageHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>



<style>

/* Import Font Dancing Script */
@import url(https://fonts.googleapis.com/css?family=Dancing+Script);

* {
	margin: 0;
}

body {
	background-color: #e8f5ff;
	font-family: Arial;
}

td {
	color: #4527a4;
}
/* Main */
.main {
	margin-top: 2%;
	margin-left: 29%;
	font-size: 28px;
	padding: 0 10px;
	width: 58%;
}

.main h2 {
	color: #4527a4;
	font-size: 24px;
	margin-bottom: 10px;
}

.main .card {
	background-color: #fff;
	border-radius: 18px;
	box-shadow: 1px 1px 8px 0 grey;
	height: auto;
	margin-bottom: 20px;
	padding: 20px 0 20px 50px;
}

.main .card table {
	border: none;
	font-size: 16px;
	height: 270px;
	width: 80%;
}

.edit {
	position: absolute;
	color: #4527a4;
	right: 14%;
}
</style>
</head>
<body class="boxed_wrapper">



	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->



	<div id="header"></div>



	<!-- banner-section -->
	<section class="banner-style-14 centred">
		<div class="container">
			<div class="content-box">
				
				<div class="image-box">
					<figure class="image-1 js-tilt">
						<img src="../images/resource/arrived.png" alt="">
					</figure>
					<h2>Your Profile</h2>
					<figure class="image-2 float-bob-x">
						<img src="../images/icons/cloud-1.png" alt="">
					</figure>
					<figure class="image-3 float-bob-x">
						<img src="../images/icons/cloud-2.png" alt="">
					</figure>
				</div>
			</div>
		</div>
	</section>
	<!-- banner-section end -->

	<!-- Main -->
	<div class="main">
		<div class="card">
			<div class="card-body">
				<i class="fa fa-pen fa-xs edit"></i>
				<table>
					<tbody>
						<tr>
							<td>Name</td>
							<td>:</td>
							<td>Deepika</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>:</td>
							<td>customer@gmail.com</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>

</div>


		<div id="footer"></div>



		<!--Scroll to top-->
		<button class="scroll-top scroll-to-target" data-target="html">
			<span class="fa fa-arrow-up"></span>
		</button>



		<!-- jequery plugins -->
		<script src="../js/jquery.js"></script>
		<script src="../js/popper.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/owl.js"></script>
		<script src="../js/wow.js"></script>
		<script src="../js/validation.js"></script>
		<script src="../js/jquery.fancybox.js"></script>
		<script src="../js/appear.js"></script>
		<script src="../js/circle-progress.js"></script>
		<script src="../js/jquery.countTo.js"></script>
		<script src="../js/scrollbar.js"></script>
		<script src="../js/jquery.paroller.min.js"></script>
		<script src="../js/tilt.jquery.js"></script>




		<!-- main-js -->
		<script src="../js/script.js"></script>
</body>
</html>