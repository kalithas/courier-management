<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>ProDelivery</title>

<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../Header.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>

</head>


<!-- page wrapper -->
<body class="boxed_wrapper">

    <!-- preloader -->
    <div class="preloader"></div>
    <!-- preloader -->

    <div id="header"></div>
    
    

    <!-- banner-section -->
    <section class="banner-style-14 centred">
        <div class="container">
            <div class="content-box">
                <h2>Welcome To ProDelivery</h2>
                <div class="text">Wanna Track Your Shipment ?</div>
                <div class="mail-box">
                    <form action = "/OC/trackConsignmentPublic" method="post">
                        <div class="form-group">
                            <input type="text" name="trackId" placeholder="Enter Your Consignment ID" required="">
                            <button type="submit">Track</button>
                        </div>
                    </form>
                </div>
                <div class="image-box">
                    <figure class="image-1 js-tilt"><img src="../images/resource/arrived.png" alt=""></figure>
                    <figure class="image-2 float-bob-x"><img src="../images/icons/cloud-1.png" alt=""></figure>
                    <figure class="image-3 float-bob-x"><img src="../images/icons/cloud-2.png" alt=""></figure>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

<div id="footer"></div>
    



<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-arrow-up"></span>
</button>


<!-- jequery plugins -->
<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>

</body>
</html>