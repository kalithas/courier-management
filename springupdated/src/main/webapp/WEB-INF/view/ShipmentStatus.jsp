<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">
<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>

<script>
	$(function() {
		$("#header").load("../CustomerPageHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>
<style>
ol.progtrckr {
	margin: 0;
	padding: 0;
	list-style-type
	none;
}

ol.progtrckr li {
	display: inline-block;
	text-align: center;
	line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li {
	width: 49%;
}

ol.progtrckr[data-progtrckr-steps="3"] li {
	width: 33%;
}

ol.progtrckr[data-progtrckr-steps="4"] li {
	width: 24%;
}

ol.progtrckr[data-progtrckr-steps="5"] li {
	width: 19%;
}

ol.progtrckr[data-progtrckr-steps="6"] li {
	width: 16%;
}

ol.progtrckr[data-progtrckr-steps="7"] li {
	width: 14%;
}

ol.progtrckr[data-progtrckr-steps="8"] li {
	width: 12%;
}

ol.progtrckr[data-progtrckr-steps="9"] li {
	width: 11%;
}

ol.progtrckr li.progtrckr-done {
	color: black;
	border-bottom: 4px solid #4527a4;
}

ol.progtrckr li.progtrckr-todo {
	color: silver;
	border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
	content: "\00a0\00a0";
}

ol.progtrckr li:before {
	position: relative;
	bottom: -2.5em;
	float: left;
	left: 50%;
	line-height: 1em;
}

ol.progtrckr li.progtrckr-done:before {
	content: "\2713";
	color: white;
	background-color: #4527a4;
	height: 2.2em;
	width: 2.2em;
	line-height: 2.2em;
	border: none;
	border-radius: 2.2em;
}

ol.progtrckr li.progtrckr-todo:before {
	content: "\039F";
	color: silver;
	background-color: white;
	font-size: 2.2em;
	bottom: -1.2em;
}

.social-box .box {
	background: #FFF;
	border-radius: 10px;
	padding: 40px 10px;
	margin: 20px 0px;
	cursor: pointer;
	transition: all 0.5s ease-out;
}

.social-box .box:hover {
	box-shadow: 0 0 6px #4183D7;
}

.social-box .box .box-text {
	margin: 20px 0px;
	font-size: 15px;
	line-height: 30px;
}

.social-box .box .box-btn a {
	text-decoration: none;
	color: #4183D7;
	font-size: 16px;
}

.fa {
	color: #4183D7;
}
</style>
</head>

<body class="boxed_wrapper">

	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>

	<!-- banner-section -->
	<section class="banner-style-14 centred">
		<div class="container">
			<div class="content-box">
				<h2>Your Shipment Status</h2>

				<div class="image-box">
					<figure class="image-1 js-tilt">
						<img src="../images/resource/arrived.png" alt="">
					</figure>
					<figure class="image-2 float-bob-x">
						<img src="../images/icons/cloud-1.png" alt="">
					</figure>
					<figure class="image-3 float-bob-x">
						<img src="../images/icons/cloud-2.png" alt="">
					</figure>
				</div>
			</div>
		</div>
	</section>
	<!-- banner-section end -->

	<ol class="progtrckr" data-progtrckr-steps="5">
		<li class="progtrckr-done">Shipment Booked</li>

		<li class="progtrckr-done">Dispatched</li>

		<li class="progtrckr-done">In transit</li>

		<li class="progtrckr-todo">Out For Delivery</li>

		<li class="progtrckr-todo">Delivered</li>
	</ol>

	<div class="social-box">
		<div class="container">
			<div class="row">

				<div class="col-lg-6 col-xs-12 text-center">
					<div class="box">
						<i class="fa fa-behance fa-3x" aria-hidden="true"></i>
						<div class="box-title">
							<h2>Shipper Information</h2>
						</div>
						<div class="box-text">
							<h4>Name:</h4>
							<h4>Address:</h4>
							<h4>Phone no.:</h4>
							<h4>Pincode:</h4>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-xs-12  text-center">
					<div class="box">
						<i class="fa fa-twitter fa-3x" aria-hidden="true"></i>
						<div class="box-title">
							<h2>Receiver Information</h2>
						</div>
						<div class="box-text">
							<h4>Name:</h4>
							<h4>Address:</h4>
							<h4>Phone no.:</h4>
							<h4>Pincode:</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	

	<div id="footer"></div>
	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>
	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>
	<!-- main-js -->
	<script src="../js/script.js"></script>

</body>
</html>