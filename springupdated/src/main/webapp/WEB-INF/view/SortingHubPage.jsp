<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">
<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../SortingHubHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>

<style>
/** banner-style-11 **/
.banner-style-11 {
	position: relative;
	padding: 340px 0px 540px 0px;
}

.banner-style-11 .vector-image .image-1 {
	position: absolute;
	left: 55px;
	top: 200px;
}

.banner-style-11 .vector-image .image-2 {
	position: absolute;
	right: 50px;
	top: 200px;
}

.banner-style-11 .image-layer {
	position: absolute;
	top: 0px;
	left: 0px;
	height: 100%;
	width: 100%;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center;
	z-index: -1;
}

.banner-style-11 .content-box {
	position: relative;
	max-width: 850px;
	margin: 0 auto;
	width: 100%;
}

.banner-style-11 .content-box h1 {
	position: relative;
	display: block;
	font-size: 48px;
	line-height: 72px;
	color: #fff;
	margin-bottom: 30px;
}

.banner-style-11 .content-box .text {
	position: relative;
	display: block;
	font-size: 20px;
	color: #fff;
	margin-bottom: 56px;
}

.banner-style-11 .content-box .btn-box .btn-one {
	position: relative;
	display: inline-block;
	overflow: hidden;
	font-size: 18px;
	color: #4527a4;
	line-height: 30px;
	font-weight: 500;
	background: #fff;
	padding: 15px 30px;
	text-align: center;
	border-radius: 30px;
	margin-right: 35px;
	z-index: 1;
}

.banner-style-11 .content-box .btn-box .btn-one:before {
	position: absolute;
	content: '';
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	opacity: 0;
	background-color: #4527a4;
	-webkit-transition: all 0.4s;
	-moz-transition: all 0.4s;
	-o-transition: all 0.4s;
	transition: all 0.4s;
	-webkit-transform: scale(0.2, 1);
	transform: scale(0.2, 1);
	z-index: -1;
}

.banner-style-11 .content-box .btn-box .btn-one:hover::before {
	opacity: 1;
	-webkit-transform: scale(1, 1);
	transform: scale(1, 1);
}

.banner-style-11 .content-box .btn-box .btn-one:hover {
	background: #4527a4;
	color: #fff;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.5);
}

.banner-style-11 .content-box .btn-box .video-btn {
	position: relative;
	display: inline-block;
	font-size: 18px;
	font-weight: 500;
	color: #fff;
	padding: 7px 0px 0px 78px;
	top: -19px;
}

.banner-style-11 .content-box .btn-box .video-btn:before {
	position: absolute;
	content: '';
	background: #fff;
	width: 105px;
	height: 1px;
	right: 0px;
	bottom: 0px;
}

.banner-style-11 .content-box .btn-box .video-btn i {
	position: absolute;
	left: 0px;
	top: 0px;
	font-size: 18px;
	font-weight: 700;
	color: #4527a4;
	width: 45px;
	height: 45px;
	line-height: 45px;
	text-align: center;
	background: #fff;
	border-radius: 50%;
	box-shadow: 0 0 0px 7px rgba(255, 255, 255, 0.2);
}
</style>
</head>
<body class="boxed_wrapper">

	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>

	<!-- banner-section -->
	<section class="banner-style-11 centred">
		<div class="vector-image">
			<figure class="image image-1 float-bob-y">
				<img src="../images/icons/deliveries.png" alt="">
			</figure>
			<figure class="image image-2 float-bob-y">
				<img src="../images/resource/deliveries.png" alt="">
			</figure>
		</div>
		<div class="image-layer"
			style="background-image: url(../images/icons/banner-bg-10.png);"></div>
		
	</section>
	<!-- banner-section end -->



	<!-- designe-process -->
	<section class="designe-process">
		<div class="container">
			<div class="sec-title center">
				<h2>Sorting Agent Page</h2>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 single-column">
					<div class="single-item wow fadeInLeft" data-wow-delay="00ms"
						data-wow-duration="1500ms">
						<div class="inner-box">
							<div class="left-layer"></div>
							<div class="right-layer"></div>
							<div class="icon-box">
								<i class="fas fa-sliders-h"></i>
							</div>
							<h4>
								<a href="/SAC/incoming">Incoming Shipments</a>
							</h4>
							
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 single-column">
					<div class="single-item wow fadeInUp" data-wow-delay="00ms"
						data-wow-duration="1500ms">
						<div class="inner-box">
							<div class="left-layer"></div>
							<div class="right-layer"></div>
							<div class="icon-box">
								<i class="fas fa-columns"></i>
							</div>
							<h4>
								<a href="/SAC/outgoing">Outgoing Shipments</a>
							</h4>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- designe-precess end -->

	<div id="footer"></div>

	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>


	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>

</body>
</html>