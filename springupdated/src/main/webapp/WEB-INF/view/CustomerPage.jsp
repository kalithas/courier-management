<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">
<!-- Stylesheets -->
<link href="../css/font-awesome-all.css" rel="stylesheet">
<link href="../css/flaticon.css" rel="stylesheet">
<link href="../css/owl.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.fancybox.min.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js">
	
</script>
<script>
	$(function() {
		$("#header").load("../CustomerPageHeader.jsp");
		$("#footer").load("../Footer.jsp");
	});
</script>

<style type="text/css">
/** ourmission-section **/
.ourmission-section {
	position: relative;
	padding: 50px 0px 205px 0px;
}

.ourmission-section .sec-title {
	margin-bottom: 60px;
}

.ourmission-section .single-item {
	position: relative;
	background: #fff;
	border-radius: 20px;
	padding: 36px 30px 48px 40px;
	margin-bottom: 15px;
	transition: all 500ms ease;
}

.ourmission-section .single-item:hover {
	box-shadow: 0 0px 15px 0px rgba(0, 0, 0, 0.1);
}

.ourmission-section .single-item .inner-box {
	position: relative;
	padding: 11px 0px 0px 220px;
}

.ourmission-section .single-item .inner-box .icon-box {
	position: absolute;
	left: 0px;
	top: 0px;
	font-size: 70px;
	color: #fff;
	width: 170px;
	height: 160px;
	line-height: 160px;
	text-align: center;
	z-index: 1;
	margin-bottom: 32px;
}

.ourmission-section .single-item .inner-box .icon-box .bg-layer {
	position: absolute;
	width: 170px;
	height: 160px;
	left: 0px;
	top: 0px;
	background-repeat: no-repeat;
	z-index: -1;
	transition: all 0.6s ease;
	-moz-transition: all 0.6s ease;
	-webkit-transition: all 0.6s ease;
	-ms-transition: all 0.6s ease;
	-o-transition: all 0.6s ease;
}

.ourmission-section .single-item:hover .inner-box .icon-box .bg-layer {
	filter: grayscale(100%);
	-webkit-filter: grayscale(100%);
	-moz-filter: grayscale(100%);
	-o-filter: grayscale(100%);
	-ms-filter: grayscale(100%);
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.ourmission-section .single-item .inner-box h3 {
	position: relative;
	display: block;
	font-size: 24px;
	line-height: 32px;
	color: #222;
	font-weight: 500;
	margin-bottom: 13px;
}

.ourmission-section .single-item .inner-box h3 a {
	display: inline-block;
	color: #222;
}

.ourmission-section .single-item .inner-box h3 a:hover {
	color: #4527a4;
}

.ourmission-section .single-item .inner-box .text {
	position: relative;
	font-size: 16px;
}
/** banner-style-15 **/
.banner-style-15 {
	position: relative;
	padding: 195px 0px 110px 0px;
}

.banner-style-15 .content-box {
	position: relative;
	max-width: 730px;
	width: 100%;
	margin: 0 auto;
	text-align: center;
	padding-bottom: 38px;
}

.banner-style-15 .content-box h1 {
	position: relative;
	font-size: 48px;
	line-height: 72px;
	font-weight: 500;
	color: #4527a4;
	margin-bottom: 24px;
}

.banner-style-15 .content-box .text {
	position: relative;
	font-size: 16px;
	line-height: 36px;
	margin-bottom: 47px;
	padding: 0px 40px;
}

.banner-style-15 .content-box .btn-box .btn-one {
	position: relative;
	display: inline-block;
	overflow: hidden;
	font-size: 16px;
	line-height: 30px;
	color: #4527a4;
	background: transparent;
	border: 1px solid #4527a4;
	text-align: center;
	padding: 14px 42px;
	border-radius: 30px;
	margin: 0px 15px;
	z-index: 1;
}

.banner-style-15 .content-box .btn-box .btn-one:before {
	position: absolute;
	content: '';
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	opacity: 0;
	background-color: #4527a4;
	-webkit-transition: all 0.4s;
	-moz-transition: all 0.4s;
	-o-transition: all 0.4s;
	transition: all 0.4s;
	-webkit-transform: scale(0.2, 1);
	transform: scale(0.2, 1);
	z-index: -1;
}

.banner-style-15 .content-box .btn-box .btn-one:hover::before {
	opacity: 1;
	-webkit-transform: scale(1, 1);
	transform: scale(1, 1);
}

.banner-style-15 .content-box .btn-box .btn-one:hover {
	background: #4527a4;
	color: #fff;
}

.banner-style-15 .content-box .btn-box .video-btn {
	position: relative;
	display: inline-block;
	overflow: hidden;
	font-size: 16px;
	line-height: 30px;
	color: #222;
	background: #e3f1f9;
	padding: 15px 80px 15px 45px;
	border-radius: 30px;
	margin: 0px 15px;
}

.banner-style-15 .content-box .btn-box .video-btn i {
	position: absolute;
	top: 0px;
	right: 0px;
	width: 60px;
	height: 60px;
	line-height: 60px;
	background: #4527a4;
	color: #fff;
	font-size: 18px;
	border-radius: 50%;
	text-align: center;
}

.banner-style-15 .content-box .btn-box .video-btn:hover {
	background: #4527a4;
	color: #fff;
}

.banner-style-15 .image-box img {
	width: 100%;
}

.banner-style-15 .anim-icons .icon-1 {
	left: 100px;
	top: 370px;
	-webkit-animation: zoom-fade 2s infinite linear;
	animation: zoom-fade 2s infinite linear;
}

.banner-style-15 .anim-icons .icon-2 {
	left: 100px;
	top: 50%;
}

.banner-style-15 .anim-icons .icon-3 {
	left: 100px;
	bottom: 0px;
}

.banner-style-15 .anim-icons .icon-4 {
	right: 100px;
	bottom: 0px;
	-webkit-animation: zoom-fade 2s infinite linear;
	animation: zoom-fade 2s infinite linear;
}

.banner-style-15 .anim-icons .icon-5 {
	right: 100px;
	top: 50%;
}

.banner-style-15 .anim-icons .icon-6 {
	right: 240px;
	top: 200px;
}

.dash_cus {
	margin-top: 150px;
	background: #ebeff2;
	padding: 50px 0px;
}

.user_info {
	background: #fff;
	padding: 20px 0px;
	box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
	overflow: hidden;
}

.user_info h2 {
	font-size: 24px;
	margin-top: 0;
	font-family: 'Montserrat', Helvetica, Arial, Lucida,
		sans-serif !important;
	padding-bottom: 5px;
	border-bottom: 1px solid #ebeff2;
}

input.form-control.cus_input:focus {
	box-shadow: none;
}

input.form-control.cus_input {
	border-radius: 0;
	/* border-color: rebeccapurple; */
}

label.col-md-3.col-form-label {
	font-size: 16px;
	font-weight: 400;
	font-family: 'Montserrat', Helvetica, Arial, Lucida,
		sans-serif !important;
}

input.form-control.cus_input_btn {
	background: #008400;
	color: #fff;
	border-radius: 0;
	border-color: #008400;
	font-family: 'Montserrat', Helvetica, Arial, Lucida,
		sans-serif !important;
}

input.form-control.cus_input_btn:hover {
	box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
}

.table {
	background: #fff;
	border-radius: 0.2rem;
	width: 150%;
	padding-bottom: 1rem;
	color: #4527a4;
	margin-bottom: 0;
}

.table th:first-child, .table
td:first-child {
	position: sticky;
	left: 0;
	background-color: #4527a4;
	color: #fff;
}

.table td {
	white-space: nowrap;
}
</style>
</head>
<body class="boxed_wrapper">

	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>

	 <!-- banner-section -->
	<section class="banner-style-15 centred">

		<div class="container">
			<div class="content-box">
				<h1></h1>
				<div class="text"></div>
			</div>
		</div>
	</section> 
	<!-- banner-section end -->
 

	<!-- ourmission-section -->
	<section class="ourmission-section">
		<div class="container">
			<div class="sec-title center">
				<h2>Welcome ${customer.customerName}</h2>
<!-- 				<p> -->
<%-- 					<span>E-mail:</span><span>${user.email}</span><br /> <span>Password:</span><span>${user.password}</span><br /> --%>
<%-- 					<span>First Name :</span><span>${customer.customerName}</span><br /> --%>
<!-- 				</p> -->
			</div>

		</div>
	</section>


	<div id="footer"></div>

	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>


	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>

	<script type="text/javascript">
		function CheckColors(val) {
			var element = document.getElementById('color');
			if (val == 'pick a color' || val == 'others')
				element.style.display = 'block';
			else
				element.style.display = 'none';
		}
	</script>
</body>
</html>

