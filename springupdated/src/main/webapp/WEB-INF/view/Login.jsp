<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>ProDelivery</title>
<!-- Fav Icon -->
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
	rel="stylesheet">


<link href="../css/style.css" rel="stylesheet">

<script>
		const toggleForm = () => {
			  const container = document.querySelector('.container');
			  container.classList.toggle('active');
			};
			</script>
			<script>
			
			function validatePassword() {
				  var x = document.getElementById("password").value;
				  var y = document.getElementById("repassword").value;
				  if (x != y) {
				    alert("Re-Enter your password");
				    return false;
				  }
				}
			
</script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>

<style>
/** =========== Login Section start============ **/
.login-section {
	position: relative;
	min-height: 100vh;
	background-color: #4527a4;
	display: flex;
	justify-content: center;
	align-items: center;
}

.login-section .container {
	position: relative;
	width: 800px;
	height: 500px;
	background: #fff;
	box-shadow: 0 15px 50px rgba(0, 0, 0, 0.1);
	overflow: hidden;
}

.login-section .container .user {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	display: flex;
}

.login-section .container .user .imgBx {
	position: relative;
	width: 60%;
	height: 100%;
	background: #ff0;
	transition: 0.5s;
}

.login-section .container .user .imgBx img {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	object-fit: cover;
}

.login-section .container .user .formBx {
	position: relative;
	width: 50%;
	height: 100%;
	background: #fff;
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 40px;
	transition: 0.5s;
}

.login-section .container .user .formBx form h2 {
	font-size: 18px;
	font-weight: 600;
	text-transform: uppercase;
	letter-spacing: 2px;
	text-align: center;
	width: 100%;
	margin-bottom: 10px;
	color: #555;
}

.login-section .container .user .formBx form input {
	position: relative;
	width: 100%;
	padding: 10px;
	background: #f5f5f5;
	color: #333;
	border: none;
	outline: none;
	box-shadow: none;
	margin: 8px 0;
	font-size: 14px;
	letter-spacing: 1px;
	font-weight: 300;
}

.login-section .container .user .formBx form input[type='submit'] {
	max-width: 100px;
	background: #4527a4;
	color: #fff;
	cursor: pointer;
	font-size: 14px;
	font-weight: 500;
	letter-spacing: 1px;
	transition: 0.5s;
}

.login-section .container .user .formBx form .signup {
	position: relative;
	margin-top: 20px;
	font-size: 12px;
	letter-spacing: 1px;
	color: #555;
	text-transform: uppercase;
	font-weight: 300;
}

.login-section .container .user .formBx form .signup a {
	font-weight: 600;
	text-decoration: none;
	color: #4527a4;
}

.login-section .container .signupBx {
	pointer-events: none;
}

.login-section .container.active .signupBx {
	pointer-events: initial;
}

.login-section .container .signupBx .formBx {
	left: 100%;
}

.login-section .container.active .signupBx .formBx {
	left: 0;
}

.login-section .container .signupBx .imgBx {
	left: -100%;
}

.login-section .container.active .signupBx .imgBx {
	left: 0%;
}

.login-section .container .signinBx .formBx {
	left: 0%;
}

.login-section .container.active .signinBx .formBx {
	left: 100%;
}

.login-section .container .signinBx .imgBx {
	left: 0%;
}

.login-section .container.active .signinBx .imgBx {
	left: -100%;
}

@media ( max-width : 991px) {
	.login-section .container {
		max-width: 400px;
	}
	.login-section .container .imgBx {
		display: none;
	}
	.login-section .container .user .formBx {
		width: 100%;
	}
}

/** Login Section end **/
</style>
</head>
<body class="boxed_wrapper">

	<!-- preloader -->
	<div class="preloader"></div>
	<!-- preloader -->

	<div id="header"></div>

	<section class="login-section">
		<div class="container">
			<div class="user signinBx">
				<div class="imgBx">
					<img src="../images/resource/login.png" alt="" />
				</div>
				<div class="formBx">
					<form method="post" >
						<h2>Sign In</h2>

						<input type = "text" name = "email" placeholder = "Email"/>
						<br />

						<input type = "password" name = "password" placeholder = "Password"/>
						<br />

							<input type="submit" name="" value="SignIn" />

						<p style="align: right">
							<a href="/OC/fp">Forgot Password</a>
						</p>
						
						<p class="signup">
							Don't have an account ? <a href="#" onclick="toggleForm();">Sign
								Up.</a>
						</p>
					</form>
				</div>
			</div>
			<div class="user signupBx">
				<div class="formBx">
					<form action = "/SUC/signup" method = "post" onsubmit = "return validatePassword()" name = "signup">
						<h2>Create an account</h2>
						<input type="text" name="userName" placeholder="Username" /> <input
							type="email" name="email" placeholder="Email Address" /> <input
							type="password" id = "password" name="password" placeholder="Create Password" /> <input
							type="password" id = "repassword" name="repassword" placeholder="Confirm Password" /> 
						<input type="submit" name="" value="Sign Up" />

						<p class="signup">
							Already have an account ? <a href="#" onclick="toggleForm();">Sign
								in.</a>
						</p>
					</form>
				</div>
				<div class="imgBx">
					<img src="../images/resource/signup.png" alt="" />
				</div>
			</div>
		</div>
	</section>

	<div id="footer"></div>


	<!--Scroll to top-->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-arrow-up"></span>
	</button>

	<!-- jequery plugins -->
	<script src="../js/jquery.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.js"></script>
	<script src="../js/wow.js"></script>
	<script src="../js/validation.js"></script>
	<script src="../js/jquery.fancybox.js"></script>
	<script src="../js/appear.js"></script>
	<script src="../js/circle-progress.js"></script>
	<script src="../js/jquery.countTo.js"></script>
	<script src="../js/scrollbar.js"></script>
	<script src="../js/jquery.paroller.min.js"></script>
	<script src="../js/tilt.jquery.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>

	<!-- main-js -->
	<script src="../js/script.js"></script>

</body>
</html>