
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- main header -->
	<header class="main-header style-two">
		<div class="outer-container">
			<div class="container">
				<div class="main-box clearfix">
					<div class="logo-box pull-left">
						<figure class="logo">
							<a href="/CC/Home"><img src="../images/logo3.png" alt=""></a>
						</figure>
					</div>
					<div class="menu-area pull-right clearfix">
						<!--Mobile Navigation Toggler-->
						<div class="mobile-nav-toggler">
							<i class="icon-bar"></i> <i class="icon-bar"></i> <i
								class="icon-bar"></i>
						</div>
						<nav class="main-menu navbar-expand-md navbar-light">
							<div class="collapse navbar-collapse show clearfix"
								id="navbarSupportedContent">
								<ul class="navigation clearfix">
									<li class=""><a href="/CC/Home">Home</a></li>
									<li class=""><a href="/CC/trackConsignment">Track
											Consignment</a></li>
									<li class=""><a href="/CC/viewConsignmentHistory">Consignment
											History</a></li>
									<li class=""><a href="/CC/giveFeedback">Give Feedback</a></li>
									<li class="dropdown"><a href="#">Profile</a>
										<ul>
											<li><a href="/CC/editProfile">Edit Profile</a></li>
											<li><a href="/CC/viewProfile">View Profile</a></li>
										</ul></li>
									<li class=""><a href="/">Logout</a></li>
								</ul>
							</div>
						</nav>

					</div>
				</div>
			</div>
		</div>



		<!--sticky Header-->
		<div class="sticky-header">
			<div class="container clearfix">
				<figure class="logo-box">
					<a href="index.html"><img src="../images/logo3.png" alt=""></a>
				</figure>
				<div class="menu-area">
					<nav class="main-menu clearfix">
						<!--Keep This Empty / Menu will come through Javascript-->
					</nav>
				</div>
			</div>
		</div>
	</header>
	<!-- main-header end -->



	<!-- Mobile Menu -->
	<div class="mobile-menu">
		<div class="menu-backdrop"></div>
		<div class="close-btn">
			<i class="fas fa-times"></i>
		</div>

		<nav class="menu-box">
			<div class="nav-logo">
				<a href="index.html"><img src="../images/logo3.png" alt=""
					title=""></a>
			</div>
			<div class="menu-outer">
				<!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
			</div>
			<div class="contact-info">
				<h4>Contact Info</h4>
				<ul>
					<li>Salarpuria, Hyderabad, India</li>
					<li><a href="#">+91 8709840503</a></li>
					<li><a href="#">prodelivery@info.com</a></li>
				</ul>
			</div>
			<div class="social-links">
				<ul class="clearfix">
					<li><a href="#"><span class="fab fa-twitter"></span></a></li>
					<li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
					<li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
					<li><a href="#"><span class="fab fa-instagram"></span></a></li>
					<li><a href="#"><span class="fab fa-youtube"></span></a></li>
				</ul>
			</div>
		</nav>
	</div>
	<!-- End Mobile Menu -->


	<script src="../js/script.js"></script>
</body>
</html>