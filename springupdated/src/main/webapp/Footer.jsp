<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- main-footer -->
	<footer class="main-footer">
		<div class="image-layer"
			style="background-image: url(../images/icons/footer-bg-4.png);"></div>
		<div class="container">
			<div class="footer-top">
				<div class="widget-section">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12 footer-column">
							<div class="about-widget footer-widget">
								<figure class="footer-logo">
									<a href="index.html"><img src="../images/logo3.png" alt=""></a>
								</figure>
								<div class="text">Welcome To ProDelivery.</div>
								<ul class="social-links">
									<li><h6>Follow Us :</h6></li>
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
									<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12 footer-column">
							<div class="links-widget footer-widget">
								<div class="widget-content">
									<ul class="list clearfix">
										<li><a href="/OC/contact">Contact Us</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12 footer-column">
							<div class="contact-widget footer-widget">
								<h4 class="widget-title">Contact Info</h4>
								<div class="widget-content">
									<ul class="list clearfix">
										<li><i class="fas fa-map-marker-alt"></i>Salarpuria, <br />Hyderabad,
											India</li>
										<li><i class="fas fa-phone-volume"></i> <a href="#">+91
												8709840503</a></li>
										<li><i class="fas fa-envelope"></i> <a
											href="mailto:info@example.com">prodelivery@info.com</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="copyright">
					&copy; 2020 <a href="#">ProDelivery</a>. All rights reserved
				</div>
			</div>
		</div>
	</footer>
	<!-- main-footer end -->
</body>
</html>