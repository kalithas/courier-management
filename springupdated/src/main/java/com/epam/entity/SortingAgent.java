package com.epam.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Entity
@Table(name = "t_sorting_agent")
@Component
@Scope("prototype")
public class SortingAgent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

///mapping to be done..

	@OneToMany(mappedBy="sortingAgent")
	private List<DeliveryPerson> deliveryPerson;

	@OneToMany(mappedBy="sortingAgent")
	private List<Consignment> consignment;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private User user;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "city_id")
	private City city;

	@Column(name = "created_by")
	private int sortingAgentCreatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "created_at")
	private Date sortingAgentCreatedAt;

	@Column(name = "updated_by")
	private int sortingAgentUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "updated_at")
	private Date sortingAgentUpdatedAt;

	@Column(name = "is_active")
	@Enumerated(EnumType.STRING)
	private Active isActive=Active.YES;
}