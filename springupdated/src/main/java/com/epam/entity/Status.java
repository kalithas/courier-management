package com.epam.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Table(name="t_status")
@Entity
public class Status {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="status_id")
	private List<StatusTrackingDetails> statusTrackingDetails;
	
	@Column(name = "code")
	private int code;
	
	@Column(name = "io_status")
	@Enumerated(EnumType.STRING)
	private IOStatus ioStatus;
	
	@Column(name = "description")
	private String description;
	
	@Column(name="created_by")
	private int customerCreaedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "created_at")
	private Date customerCreatedAt;
	
	@Column(name="updated_by")
	private int customerUpdatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "updated_at")
	private Date customerUpdatedAt;
	
	@Column(name="is_active")
    @Enumerated(EnumType.STRING)
	private Active isActive=Active.YES;
	
}


