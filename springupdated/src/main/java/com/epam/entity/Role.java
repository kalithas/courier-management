package com.epam.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
@Table(name="t_role")
public class Role 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="code")
	private String code;
	
	@Column(name="description")
	private String description;
	

	@Column(name="created_by")
	private int consignmentCreatedBy;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name="created_at")
	private Date consignmentCreatedAt;
	
	@Column(name="updated_by")
	private int consignmentUpdatedBy;
	
	
	@Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
	@Column(name="updated_at")
	private Date consignmentUpdatedAt;
	
	@Column(name="is_active")
    @Enumerated(EnumType.STRING)
	private Active isActive=Active.YES;
}
