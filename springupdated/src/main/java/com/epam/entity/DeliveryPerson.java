package com.epam.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;


@Data
@Entity
@Table(name="t_delivery_person")
public class DeliveryPerson 
{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@OneToOne(cascade=CascadeType.ALL)
	private User user;

	//	
	//	@OneToMany(cascade = CascadeType.ALL)
	//	private List<Consignment> consignment;

	//	@OneToMany(fetch = FetchType.LAZY)
	//	 @JoinColumn(name = "delivery_id")
	//	private List<Consignment> consignment;



	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "delivery_person_id")
	private List<Consignment> consignment;

	@Column(name="name")
	private String deliveryPersonName;

	@Column(name="created_by")
	private int deliveryPersonCreatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "created_at")
	private Date deliveryPersonCreatedAt; 

	@Column(name="updated_by")
	private int deliveryPersonUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "updated_at")
	private Date deliveryPersonUpdatedAt;

	@Column(name="is_active")
	@Enumerated(EnumType.STRING)
	private Active isActive=Active.YES;
	
	@ManyToOne
	@JoinColumn(name="sorting_agent_id")
	private SortingAgent sortingAgent;
	
}
