package com.epam.entity;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import lombok.Data;

@Data
@Entity
@Table(name = "t_consignment")
public class Consignment { // con con.getCustomer()
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "status_id")
	private Status status;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "sorting_agent_id")
	private SortingAgent sortingAgent;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "branch_id")
	private Branch branch;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "air_hub_id")
	private AirHub airHub;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "delivery_person_id")
	private DeliveryPerson deliveryPerson;

	@Column(name = "source")
	private String source;

	@Column(name = "destination")
	private String destination;

	@Column(name = "expected_delivery")
	private Date expectedDelivery;

	@Column(name = "created_by")
	private int consignmentCreatedBy;

	@Column(name = "mode_of_delivery")
	private String deliveryMode;

	@Column(name = "tracking_id")
	private String trackingId;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "created_at")
	private Date consignmentCreatedAt;

	@Column(name = "updated_by")
	private int consignmentUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "updated_at")
	private Date consignmentUpdatedAt;

	@Column(name = "is_active")
	@Enumerated(EnumType.STRING)
	private Active isActive = Active.YES;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Column(name = "source_address")
	private String sourceAddress;

	@Column(name = "destination_address")
	private String destinationAddress;

	@Column(name = "source_zipcode")
	private int sourceZipcode;

	@Column(name = "destination_zipcode")
	private int destinationZipcode;

	@Column(name = "weight")
	private double weight;

	@Column
	private double price;
}