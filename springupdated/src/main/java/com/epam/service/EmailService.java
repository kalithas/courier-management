package com.epam.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.epam.entity.MailRequest;
import com.epam.entity.MailResponse;

@Service
public interface EmailService {
	public MailResponse sendEmail(MailRequest request, Map<String, Object> model,int emailType);
}