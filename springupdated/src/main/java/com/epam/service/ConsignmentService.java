package com.epam.service;


import java.util.List;

import org.springframework.stereotype.Service;
import com.epam.vo.ConsignmentVO;

@Service
public interface ConsignmentService {
	public ConsignmentVO trackConsignment(String trackId);
	public String findTrackingId(String string);
	public List<ConsignmentVO> trackConsignmentAllStatus(String trackId);
}