package com.epam.service;

import java.io.FileNotFoundException;
import java.util.List;
import org.springframework.stereotype.Service;
import com.epam.entity.Branch;
import com.epam.entity.Consignment;
import com.epam.entity.Feedback;

import net.sf.jasperreports.engine.JRException;

@Service
public interface AdminService {
	public boolean updateBranch(String branchName, String zipCodeFirst, String address, String zipCode, String isActive);
	public int addBranch(int createdBy,String email,String password,String address,String pinCode,String city);
	public int addSortingAgent(int createdBy, String email, String password,String cityName);
	public int addAirHub(int createdBy, String email, String password,String cityName);
	public int getBranchId(int zipCode);
	public List<Feedback> getAllFeedbacks();
	public List<Branch> getAllBranches();
	public List<Consignment> generateReport(int month, int year, int zipcode) throws FileNotFoundException, JRException;
	public List<String> getAllBranchCityWithZipcode();
}
