package com.epam.service;

import org.springframework.stereotype.Service;

@Service
public interface UserService {
	public void sendMail(String mail);
	public void changePassword(String password,String mail);
	public int getUserId(String mail);
}
