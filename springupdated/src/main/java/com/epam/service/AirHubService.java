package com.epam.service;

import java.util.List;

import com.epam.entity.Consignment;
import com.epam.vo.ConsignmentVO;

public interface AirHubService {
	public List<ConsignmentVO> getIncomingConsignmentList(int id);
	public void receiveConsignment(String trackingId, int sortingAgentId);
	public void sendConsignment(String trackingId, int airHubId);
	public int findByUserId(int airHubUserId);
	public List<Consignment> ListOfOutgoingConsignment(int id);
}
