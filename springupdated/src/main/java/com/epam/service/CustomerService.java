package com.epam.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.vo.ConsignmentVO;

@Service
public interface CustomerService {
	public List<ConsignmentVO> getConsignmentList(int customerId);
	public String giveFeedback(String consignmentId,String email, String description);
}
