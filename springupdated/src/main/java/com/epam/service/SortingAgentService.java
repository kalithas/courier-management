package com.epam.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.entity.Consignment;
import com.epam.vo.ConsignmentVO;

@Service
public interface SortingAgentService {
	public void receiveConsignments(String trackingId,int sortingAgentId);
	public void addDeliveryPerson(int id,String password,String name,String mail);
	public void sendConsignment(String trackingId, int sortingAgentId);
	public List<ConsignmentVO> findByCity(int saId);
	public List<ConsignmentVO> getListOfIncomingConsignments(int sortingAgentId);
	public List<Consignment> ListOfOutgoingConsignment(int id) ;
}
