package com.epam.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.entity.City;
import com.epam.repo.CityRepo;
import com.epam.service.CityService;

@Service
public class CityServiceImpl implements CityService{

	@Autowired
	CityRepo repo;

	@Override
	public List<City> getCities() {

		return repo.findAll();

	}

}
