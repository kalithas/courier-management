package com.epam.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.entity.Active;
import com.epam.entity.Admin;
import com.epam.entity.Branch;
import com.epam.entity.Customer;
import com.epam.entity.DeliveryPerson;
import com.epam.entity.Role;
import com.epam.entity.User;
import com.epam.entity.UserRole;
import com.epam.repo.AdminRepo;
import com.epam.repo.BranchRepo;
import com.epam.repo.CustomerRepo;
import com.epam.repo.DeliveryPersonRepo;
import com.epam.repo.LoginRepo;
import com.epam.repo.RoleRepo;
import com.epam.repo.UserRepo;
import com.epam.repo.UserRoleRepo;
import com.epam.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {
	private static final Logger logger = LogManager.getLogger(LoginServiceImpl.class);
	@Autowired
	LoginRepo loginRepo;

	@Autowired
	CustomerRepo customerRepo;

	@Autowired
	UserRoleRepo userRoleRepo;

	@Autowired
	AdminRepo adminRepo;

	@Autowired
	BranchRepo branchRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	RoleRepo roleRepo;

	@Autowired
	DeliveryPersonRepo deliveryPersonRepo;

	@Override
	public List<User> checkLogin(String email, String password) {
		List<User> user = new ArrayList<User>();
		loginRepo.checkLogin(email, password).forEach(t1 -> user.add(t1));
		return user;
	}

	@Override
	public List<UserRole> checkRole(int id) {
		List<UserRole> ur = new ArrayList<UserRole>();
		userRoleRepo.checkRole(id).forEach(t1 -> ur.add(t1));
		logger.info(ur);
		return ur;
	}

	@Override
	public List<Customer> getCustomerDetails(int id) {
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerDetails(id).forEach(t1 -> customer.add(t1));
		return customer;
	}

	@Override
	public List<Admin> getAdminDetails(int id) {
		List<Admin> admin = new ArrayList<Admin>();
		adminRepo.getAdminDetails(id).forEach(t1 -> admin.add(t1));
		return admin;
	}

	@Override
	public List<Branch> getBranchDetails(int id) {
		List<Branch> branchs = new ArrayList<Branch>();
		branchRepo.getBranchDetails(id).forEach(b -> branchs.add(b));
		return branchs;

	}

	@Override
	public User checkPassword(String email) {
		User user = userRepo.findByEmail(email);
		return user;
	}

	@Override
	public boolean setPassword(int id, String password) {
		User user = userRepo.findId(id);
		user.setPassword(password);
		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			return false;

		}

	}

	@Override
	public boolean saveUser(String email, String password, String userName) {
		try {
			int customerUserId=5;
			User user = new User();
			user.setEmail(email);
			user.setPassword(password);
			userRepo.save(user);
			int role_id = roleRepo.getRole("Customer");
			Role r = roleRepo.getById(role_id);
			UserRole ur = new UserRole();
			ur.setUser(user);
			ur.setRole(r);
			userRoleRepo.save(ur);
			Customer cust = new Customer();
			cust.setUser(user);
			cust.setCustomerName(userName);
			cust.setCustomerCreaedBy(customerUserId);
			cust.setCustomerUpdatedBy(customerUserId);
			cust.setIsActive(Active.YES);
			customerRepo.save(cust);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updatePassword(String mail, String oldPassword, String newPassword) {
		User user = userRepo.findByEmail(mail);
		String currentPassword = user.getPassword();
		if (currentPassword.equals(oldPassword)) {
			user.setPassword(newPassword);
			user.setUserUpdatedBy(user.getId());
			userRepo.save(user);
			return true;
		}

		return false;
	}

	@Override
	public List<DeliveryPerson> getDeliveryPersonDetails(int id) {
		List<DeliveryPerson> deliveryPersons = new ArrayList<DeliveryPerson>();
		deliveryPersonRepo.getDeliveryPersonDetails(id).forEach(d -> deliveryPersons.add(d));
		return deliveryPersons;
	}

}
