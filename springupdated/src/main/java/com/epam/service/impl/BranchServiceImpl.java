package com.epam.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.entity.Active;
import com.epam.entity.Branch;
import com.epam.entity.Consignment;
import com.epam.entity.Customer;
import com.epam.entity.IOStatus;
import com.epam.entity.Role;
import com.epam.entity.Status;
import com.epam.entity.StatusTrackingDetails;
import com.epam.entity.User;
import com.epam.entity.UserRole;
import com.epam.repo.BranchRepo;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.CustomerRepo;
import com.epam.repo.RoleRepo;
import com.epam.repo.StatusRepo;
import com.epam.repo.StatusTrackingDetailsRepo;
import com.epam.repo.UserRepo;
import com.epam.repo.UserRoleRepo;
import com.epam.service.BranchService;
import com.epam.util.CookieUtil;
import com.epam.util.CourierUtil;

@Service
public class BranchServiceImpl implements BranchService {
	private static final Logger logger = LogManager.getLogger(BranchServiceImpl.class);
	@Autowired
	BranchRepo branchRepo;
	
	@Autowired
	UserRole userRole;
	
	@Autowired
	UserRoleRepo userRoleRepo;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	RoleRepo roleRepo;
	
	@Autowired
	CustomerRepo customerRepo;
	
	@Autowired
	ConsignmentRepo consignmentRepo;
	
	@Autowired
	StatusRepo statusRepo;
	
	@Autowired
	StatusTrackingDetailsRepo statusTrackingDetailsRepo;
	
	@Autowired
	CookieUtil cookieUtil;

	@Override
	public List<String> generateReport(int month, int year, int branchId){
		List<String> consignmentList = branchRepo.getMonthlyBranchConsignments(month, year, branchId);
		return consignmentList;
	}

	@SuppressWarnings("unused")
	@Override
	public String setDetails(String name, String email, String source, String destination, String modeofdelivery,
			int userId, String sourceAddress, String destinationAddress, int sourceZipCode, int destinationZipCode,
			double weight, double price) {
		Consignment consignment = new Consignment();
		int branchUserId = cookieUtil.getUserId();
		Date dt = new Date();
		Date deliveryDateAirways = new Date(dt.getTime() + (1000 * 60 * 60 * 96));
		Date deliveryDateLandways = new Date(dt.getTime() + (1000 * 60 * 60 * 72));
		if (modeofdelivery.equalsIgnoreCase("airways"))
			consignment.setExpectedDelivery(deliveryDateAirways);
		else {
			consignment.setExpectedDelivery(deliveryDateLandways);
		}
		String trackString = CourierUtil.getPassword();
		logger.info("Mode of delivery " + modeofdelivery);
		consignment.setSource(source);
		consignment.setDestination(destination);
		consignment.setDeliveryMode(modeofdelivery);
		consignment.setConsignmentCreatedBy(userId);//
		consignment.setConsignmentUpdatedBy(userId);//
		consignment.setTrackingId(trackString);
		consignment.setSourceAddress(sourceAddress);
		consignment.setDestinationAddress(destinationAddress);
		consignment.setSourceZipcode(sourceZipCode);
		consignment.setDestinationZipcode(destinationZipCode);
		consignment.setWeight(weight);
		consignment.setPrice(price);
		Status status = new Status();
		status.setIoStatus(IOStatus.OUTGOING);
		status.setCode(userId);
		status.setCustomerCreaedBy(branchUserId);
		statusRepo.save(status);
		StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
		sTrackingDetails.setCode(status.getCode());
		sTrackingDetails.setIoStatus(status.getIoStatus());
		sTrackingDetails.setDescription("Consignmnet at branch. Will be shipped");
		statusTrackingDetailsRepo.save(sTrackingDetails);
		List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
		statustrackingdetailsList.add(sTrackingDetails);
		status.setStatusTrackingDetails(statustrackingdetailsList);
		sTrackingDetails.setStatus(status);
		sTrackingDetails.setStatusIdFinal(status.getId());
		Branch b = branchRepo.findByUserId(userId);
		consignment.setStatus(status);
		consignment.setBranch(b);
		logger.info("email here " + email);
		User user = userRepo.findByEmail(email);
		logger.info("user here " + user);
		logger.info("Entered setDetails method");
		if (user != null) {
			int uId = user.getId();
			List<Consignment> cList = new ArrayList<Consignment>();
			cList.add(consignment);
			Customer customer = customerRepo.findByUser(user);
			customer.setConsignment(cList);
			consignment.setCustomer(customer);
			customer.setUser(user);
			logger.info("Inside if");
			try {
				consignmentRepo.save(consignment);
				return "Success";
			} catch (Exception e) {
				return "Error";
			}
		} else {
			logger.info("new user");
			User newUser = new User();
			newUser.setEmail(email);
			String password = CourierUtil.getPassword();
			newUser.setPassword(password);
			userRepo.save(newUser);
			int uId = newUser.getId();
			List<Consignment> cList = new ArrayList<Consignment>();
			cList.add(consignment);
			Customer customer = new Customer();
			customer.setCustomerCreaedBy(branchUserId);
			customer.setCustomerUpdatedBy(branchUserId);
			customer.setCustomerName(name);
			userRole.setIsActive(Active.YES);
			customer.setUser(newUser);
			userRole.setUser(newUser);
			logger.info("user id created" + uId);
			userRole.setIsActive(Active.YES);
			Role role = roleRepo.findByCode("Customer");
			userRole.setRole(role);
			customer.setConsignment(cList);
			userRoleRepo.save(userRole);
			try {
				customerRepo.save(customer);
				Customer customer1 = customerRepo.findByUserId(uId);
				consignment.setCustomer(customer1);
				consignmentRepo.save(consignment);
				// mail module called using apsect
				return "Success";
			} catch (Exception e) {
				return "Error";
			}
		}
	}

	@Override
	public double calculatePrice(double weight) {
		int wt = (int) weight;
		logger.info("weight here " + weight);
		double price = 30 + (wt / 50) * 5;
		logger.info("price here " + price);
		return price;
	}
}