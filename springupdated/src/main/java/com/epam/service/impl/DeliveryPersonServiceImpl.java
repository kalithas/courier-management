package com.epam.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.entity.Consignment;
import com.epam.entity.Customer;
import com.epam.entity.DeliveryPerson;
import com.epam.entity.IOStatus;
import com.epam.entity.Status;
import com.epam.entity.StatusTrackingDetails;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.StatusRepo;
import com.epam.repo.StatusTrackingDetailsRepo;
import com.epam.service.DeliveryPersonService;
import com.epam.vo.ConsignmentVO;

@Service
public class DeliveryPersonServiceImpl implements DeliveryPersonService {
	private static final Logger logger = LogManager.getLogger(DeliveryPersonServiceImpl.class);
	@Autowired
	ConsignmentRepo consignmentRepo;

	@Autowired
	StatusRepo statusRepo;

	@Autowired
	StatusTrackingDetailsRepo statusTrackingDetailsRepo;
	@Override
	public void deliverConsignment(int consignmentId) {

		 logger.info("entered deliverConsignment");

		Consignment c = consignmentRepo.findById(consignmentId);
		Customer cus = c.getCustomer();
		Status s = c.getStatus();
		StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
		s.setCode(cus.getUser().getId()); // userId
		s.setDescription("Delivered");
		s.setIoStatus(IOStatus.DELIVERED);
		statusRepo.save(s);
		sTrackingDetails.setStatusIdFinal(s.getId());
		c.setStatus(s);
		consignmentRepo.save(c);
		sTrackingDetails.setCode(s.getCode());
		sTrackingDetails.setIoStatus(s.getIoStatus());
		sTrackingDetails.setDescription("Consigment Delivered !!");
		statusTrackingDetailsRepo.save(sTrackingDetails);
		List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
		statustrackingdetailsList.add(sTrackingDetails);
		sTrackingDetails.setStatus(s);
		s.setStatusTrackingDetails(statustrackingdetailsList);
	}

	@Override
	public List<ConsignmentVO> getConsignmentList(int deliveryPersonId) {

		List<Consignment> l = consignmentRepo.findByDeliveryPerson(deliveryPersonId);
		List<ConsignmentVO> lvo = new ArrayList<ConsignmentVO>();
		for (Consignment c : l) {
			if (!c.getStatus().getIoStatus().equals(IOStatus.DELIVERED)) {
				ConsignmentVO cvo = new ConsignmentVO();
				cvo.setId(c.getId());
				cvo.setConsignmentCreatedAt(c.getConsignmentCreatedAt());
				cvo.setSource(c.getSource());
				cvo.setDestination(c.getDestination());
				cvo.setTrackingId(c.getTrackingId());
				cvo.setStatus(c.getStatus());
				lvo.add(cvo);
			}
		}

		return lvo;
	}

	@Override
	public int assignConsignment(Consignment consignment, List<DeliveryPerson> dList, int sortingagentId) {

		int deliveryID = 0;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

		int zipcode = consignment.getDestinationZipcode();
		List<Integer> unassinedDeliveryPersons = new ArrayList<Integer>();

		 logger.info("dlist" + dList.size());

		for (DeliveryPerson d : dList) {
			Integer checkString = statusRepo.findStatusByCode(d.getUser().getId());
			 logger.info("checkstring" + checkString);
			if (checkString != null) {
				Consignment consignment1 = consignmentRepo.findByStatus(checkString);
				 logger.info("entered if");
				map.put(consignment1.getDestinationZipcode(), d.getId());
				 logger.info(consignment1.getDestinationZipcode());
			} else {
				unassinedDeliveryPersons.add(d.getId());
				 logger.info("enter else");
			}
		}

		if (map.containsKey(zipcode)) {
			deliveryID = map.get(zipcode);
			 logger.info("map contains zipcode" + zipcode);
		} else if (unassinedDeliveryPersons.size() > 0) {
			for (int i = 0; i < unassinedDeliveryPersons.size(); i++) {
				Boolean res = map.containsValue(unassinedDeliveryPersons.get(i));
				if (res == false) {
					 logger.info("inside unassigne dfir loop" + unassinedDeliveryPersons.get(i));
					deliveryID = unassinedDeliveryPersons.get(i);
					break;
				}

			}
		}
		 logger.info("unassigned delivery person size:" + unassinedDeliveryPersons.size());
		map.entrySet().forEach(s -> {
			 logger.info("map valuess " + s.getKey() + "," + s.getValue());
		});

		return deliveryID;
	}

}
