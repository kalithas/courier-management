package com.epam.service.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.entity.Active;
import com.epam.entity.AirHub;
import com.epam.entity.Branch;
import com.epam.entity.City;
import com.epam.entity.Consignment;
import com.epam.entity.Feedback;
import com.epam.entity.Role;
import com.epam.entity.SortingAgent;
import com.epam.entity.User;
import com.epam.entity.UserRole;
import com.epam.repo.AirHubRepo;
import com.epam.repo.BranchRepo;
import com.epam.repo.CityRepo;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.FeedbackRepo;
import com.epam.repo.RoleRepo;
import com.epam.repo.SortingAgentRepo;
import com.epam.repo.UserRepo;
import com.epam.repo.UserRoleRepo;
import com.epam.service.AdminService;
import com.epam.util.CookieUtil;
import net.sf.jasperreports.engine.JRException;

@Service
public class AdminServiceImpl implements AdminService {
	private static final Logger logger = LogManager.getLogger(AdminServiceImpl.class);
	@Autowired
	BranchRepo branchRepo;

	@Autowired
	FeedbackRepo feedbackRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	RoleRepo roleRepo;

	@Autowired
	UserRoleRepo userRoleRepo;

	@Autowired
	CityRepo cityRepo;

	@Autowired
	SortingAgentRepo sortingAgentRepo;

	@Autowired
	AirHubRepo airHubRepo;

	@Autowired
	CookieUtil cookieUtil;

	@Autowired
	ConsignmentRepo consignmentRepo;

	public AdminServiceImpl(BranchRepo branchRepo2) {
		this.branchRepo = branchRepo2;
	}

	@Override
	public boolean updateBranch(String branchName, String zipCodeFirst, String address, String zipCode,
			String isActive) {
		// TODO Auto-generated method stub

		Branch branch = branchRepo.getId(zipCodeFirst, branchName);

		if (address.length() == 0) {
			branch.setZipCode(Integer.parseInt(zipCode));
			branch.setIsActive(Active.valueOf(isActive.toUpperCase()));
			logger.info("enetred if");

		} else if (zipCode.length() == 0) {
			branch.setAddress(address);
			branch.setIsActive(Active.valueOf(isActive.toUpperCase()));
			logger.info("enetred else if");

		} else {
			branch.setAddress(address);
			branch.setZipCode(Integer.parseInt(zipCode));
			branch.setIsActive(Active.valueOf(isActive.toUpperCase()));
			logger.info("enetred else ");

		}
		try {
			branch.setBranchUpdatedBy(cookieUtil.getUserId());
			branchRepo.save(branch);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	@Override
	public List<Feedback> getAllFeedbacks() {
		List<Feedback> feedbacks = new ArrayList<Feedback>();
		feedbackRepo.findAll().forEach(feedback -> feedbacks.add(feedback));
		return feedbacks;
	}

	private City getCity(String cityName) {
		City city;
		try {
			int id = cityRepo.getIDofCity(cityName);
			city = cityRepo.getById(id);
		} catch (Exception e) {
			city = new City();
			city.setCityName(cityName);
			city = cityRepo.save(city);
		}

		return city;
	}

	private User saveAndGetUser(String email, int createdBy, String password, String role) {

		logger.info("Role ===== " + role);

		UserRole userRole = new UserRole();
		int roleId = roleRepo.getRole(role);
		User user = new User();
		user.setPassword(password);
		user.setEmail(email);
		user.setUserCreatedBy(createdBy);
		user.setUserUpdatedBy(createdBy);
		user.setIsActive(Active.YES);

		User temp = userRepo.save(user);
		Role r = roleRepo.getById(roleId);

		userRole.setUser(user);
		userRole.setRole(r);
		userRole.setUserRoleCreaedBy(createdBy);
		userRole.setUserRoleUpdatedBy(createdBy);
		userRole.setIsActive(Active.YES);

		userRoleRepo.save(userRole);

		logger.info("user" + userRole.toString());
		return temp;
	}

	@Override
	public int addBranch(int createdBy, String email, String password, String address, String zipCode,
			String cityName) {
		String role = "Branch";
		City city = getCity(cityName);
		try {
			User user = saveAndGetUser(email, createdBy, password, role);
			Branch branch = new Branch();
			branch.setBranchCreatedBy(createdBy);
			branch.setBranchUpdatedBy(createdBy);
			branch.setUser(user);
			branch.setIsActive(Active.YES);
			branch.setZipCode(Integer.parseInt(zipCode));
			branch.setAddress(address);
			branchRepo.save(branch);
			city.addBranch(branch);
			city.setCustomerCreatedBy(createdBy);
			city.setCustomerUpdatedBy(createdBy);
			cityRepo.save(city);
			return user.getId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int addSortingAgent(int createdBy, String email, String password, String cityName) {
		String role = "Sorting Agent";
		City city = getCity(cityName);
		try {
			User user = saveAndGetUser(email, createdBy, password, role);
			SortingAgent sortingAgent = new SortingAgent();
			sortingAgent.setUser(user);
			sortingAgent.setSortingAgentCreatedBy(createdBy);
			sortingAgent.setIsActive(Active.YES);
			sortingAgent.setSortingAgentUpdatedBy(createdBy);
			SortingAgent tempSortingAgent = sortingAgentRepo.save(sortingAgent);
			logger.info("Sorting agent ==== " + tempSortingAgent);
			tempSortingAgent.setCity(city);
			cityRepo.save(city);
			return user.getId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int addAirHub(int createdBy, String email, String password, String cityName) {

		String role = "AirHub";
		City city = getCity(cityName);

		try {
			User user = saveAndGetUser(email, createdBy, password, role);
			AirHub airHub = new AirHub();
			airHub.setUser(user);
			airHub.setAirHubCreatedBy(createdBy);
			airHub.setIsActive(Active.YES);
			airHub.setAirHubUpdatedBy(createdBy);
			AirHub tempAirHub = airHubRepo.save(airHub);
			logger.info("Sorting agent ==== " + tempAirHub);
			tempAirHub.setCity(city);
			cityRepo.save(city);

			return user.getId();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public List<Branch> getAllBranches() {
		// TODO Auto-generated method stub
		List<Branch> branches = branchRepo.findAll();
		return branches;
	}

	@Override
	public int getBranchId(int zipCode) {
		// TODO Auto-generated method stub
		int branch_id = branchRepo.findByZipcode(zipCode);
		logger.info(branch_id);
		return branch_id;
	}

	@Override
	public List<Consignment> generateReport(int month, int year, int zipcode)
			throws FileNotFoundException, JRException {
		logger.info("Consignment Report generator started...");
		logger.info("file generating..");
		List<Consignment> consignmentList = consignmentRepo.getMonthlyCityConsignments(month, year, zipcode);
		if (consignmentList == null)
			logger.info("null");
		else
			logger.info("not null");
		return consignmentList;

	}

	public List<String> getAllBranchCityWithZipcode() {
		List<String> str = consignmentRepo.getAllCityWithZipcode();
		return str;
	}

}
