package com.epam.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.entity.AirHub;
import com.epam.entity.Branch;
import com.epam.entity.City;
import com.epam.entity.Consignment;
import com.epam.entity.Customer;
import com.epam.entity.Feedback;
import com.epam.entity.SortingAgent;
import com.epam.entity.User;
import com.epam.repo.AirHubRepo;
import com.epam.repo.BranchRepo;
import com.epam.repo.CityRepo;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.CustomerRepo;
import com.epam.repo.FeedbackRepo;
import com.epam.repo.SortingAgentRepo;
import com.epam.repo.UserRepo;
import com.epam.repo.UserRoleRepo;
import com.epam.service.CustomerService;
import com.epam.vo.ConsignmentVO;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepo customerRepo;

	@Autowired
	AirHubRepo airHubRepo;

	@Autowired
	SortingAgentRepo sortingAgentRepo;

	@Autowired
	BranchRepo branchRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	CityRepo cityRepo;

	@Autowired
	ConsignmentRepo consignmentRepo;

	@Autowired
	FeedbackRepo feedbackRepo;

	@Autowired
	UserRoleRepo userRoleRepo;

	@Override
	public List<ConsignmentVO> getConsignmentList(int customerId) {

		Customer customer = customerRepo.findById(customerId);

		List<Consignment> consignmentList = customer.getConsignment();

		List<ConsignmentVO> consignmentVOList = new ArrayList<ConsignmentVO>();
		for (Consignment c : consignmentList) {
			ConsignmentVO cvo = new ConsignmentVO();
			cvo.setConsignmentCreatedAt(c.getConsignmentCreatedAt());
			cvo.setDestination(c.getDestination());
			cvo.setSource(c.getSource());
			cvo.setTrackingId(c.getTrackingId());
			cvo.setModeOfDelivery(c.getDeliveryMode());

			cvo.setStatus(c.getStatus());
			int statusCode = c.getStatus().getCode();

			Integer role_id = userRoleRepo.findRoleIdByUserId(statusCode);
			// write case for null
			if(role_id==null)
			{
				cvo.setCurrentEntity("Customer");
				City city = cityRepo.findByCityName(c.getSource());
				cvo.setCity(city);
			}
			else if (role_id == 2) {
				cvo.setCurrentEntity("Branch");
				Branch branch = branchRepo.findByUserId(statusCode);
				cvo.setCity(branch.getCity());
			} else if (role_id == 3) {
				cvo.setCurrentEntity("Sorting Hub");
				SortingAgent sortingAgent = sortingAgentRepo.findByUserId(statusCode);
				cvo.setCity(sortingAgent.getCity());
			} else if (role_id == 4) {
				cvo.setCurrentEntity("Air Hub");
				AirHub airHub = airHubRepo.findByUserId(statusCode);
				cvo.setCity(airHub.getCity());
			} else if (role_id == 5) {
				cvo.setCurrentEntity("Customer");
				City city = cityRepo.findByCityName(c.getDestination());
				cvo.setCity(city);
			} else if (role_id == 6) {
				cvo.setCurrentEntity("Delivery Person");
				City city = cityRepo.findByCityName(c.getDestination());
				cvo.setCity(city);
			}
			consignmentVOList.add(cvo);
		}

		return consignmentVOList;
	}

	@Override
	public String giveFeedback(String consignmentId, String email, String description) {
		User user = userRepo.findByEmail(email);

		Feedback feedback = new Feedback();

		int id = user.getId();
		Customer customer = customerRepo.findByUserId(id);

		Consignment consignment = consignmentRepo.findByTrackingId(consignmentId);
		Feedback checkIfFeedbackGiven = feedbackRepo.findByCustomerAndConsignemntId(customer.getId(),
				consignment.getId());
		if (checkIfFeedbackGiven == null) {
			feedback.setFeedbackDescription(description);
			feedback.setCustomer(customer);
			feedback.setConsignment(consignment);
			feedback.setStatus("YES");
			feedbackRepo.save(feedback);
			return "success";
		} else if (!(checkIfFeedbackGiven.getStatus().equals("YES"))) {

			feedback.setFeedbackDescription(description);
			feedback.setCustomer(customer);
			feedback.setConsignment(consignment);
			feedback.setStatus("YES");

			try {
				feedbackRepo.save(feedback);
				return "success";

			} catch (Exception e) {
				return "error";
			}
		} else {
			return "error";
		}

	}

}
