package com.epam.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.entity.MailRequest;
import com.epam.entity.User;
import com.epam.repo.UserRepo;
import com.epam.repo.UserRoleRepo;
import com.epam.service.EmailService;
import com.epam.service.UserService;
import com.epam.util.CourierUtil;

@Service
public class UserServiceImpl implements UserService {
	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	UserRoleRepo userRoleRepo;
	
	@Override
	public void sendMail(String mail) {
		int emailType=2;
		 logger.info("inside sendMail");
		int userId = getUserId(mail);
		 logger.info(userId);
		String otp = CourierUtil.sendOtp(userId);
		
		Map<String, Object> model = new HashMap<>();
		model.put("Name", "ProDeliver");
		model.put("Location", "Bangalore,India");
		model.put("OTP", otp);
		MailRequest request = new MailRequest();
		request.setFrom("oceandreams335@gmail.com");
		request.setTo(mail);
		request.setSubject("change password OTP");
		emailService.sendEmail(request, model,emailType);
		 logger.info("OTP here "+otp);
		 logger.info("mail sent");
	}

	@Override
	public int getUserId(String mail) {
		 logger.info("=== Mail is ===  "+mail );
		return userRepo.findByEmail(mail).getId();
	}

	@Override
	public void changePassword(String password, String mail) {
		User user = userRepo.findByEmail(mail);
		user.setPassword(password);
		userRepo.save(user);
	}

}