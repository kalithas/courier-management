package com.epam.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.entity.Active;
import com.epam.entity.AirHub;
import com.epam.entity.Branch;
import com.epam.entity.City;
import com.epam.entity.Consignment;
import com.epam.entity.DeliveryPerson;
import com.epam.entity.IOStatus;
import com.epam.entity.Role;
import com.epam.entity.SortingAgent;
import com.epam.entity.Status;
import com.epam.entity.StatusTrackingDetails;
import com.epam.entity.User;
import com.epam.entity.UserRole;
import com.epam.repo.AirHubRepo;
import com.epam.repo.BranchRepo;
import com.epam.repo.CityRepo;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.DeliveryPersonRepo;
import com.epam.repo.RoleRepo;
import com.epam.repo.SortingAgentRepo;
import com.epam.repo.StatusRepo;
import com.epam.repo.StatusTrackingDetailsRepo;
import com.epam.repo.UserRepo;
import com.epam.repo.UserRoleRepo;
import com.epam.service.DeliveryPersonService;
import com.epam.service.SortingAgentService;
import com.epam.vo.ConsignmentVO;

@Service
public class SortingAgentServiceImpl implements SortingAgentService {
	private static final Logger logger = LogManager.getLogger(SortingAgentServiceImpl.class);
	@Autowired
	ConsignmentRepo consignmentRepo;

	@Autowired
	StatusRepo statusRepo;

	@Autowired
	SortingAgentRepo sortingAgentRepo;

	@Autowired
	BranchRepo branchRepo;

	@Autowired
	DeliveryPersonService deliveryPersonService;

	@Autowired
	AirHubRepo airHubRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	RoleRepo roleRepo;

	@Autowired
	UserRoleRepo userRoleRepo;

	@Autowired
	DeliveryPersonRepo deliveryPersonRepo;

	@Autowired
	AdminServiceImpl adminServiceImpl;

	@Autowired
	CityRepo cityRepo;
	
	@Autowired
	StatusTrackingDetailsRepo statusTrackingDetailsRepo;

	@Override
	public List<ConsignmentVO> findByCity(int saID) {
		List<Consignment> clist = consignmentRepo.findConsignmentsByCity(saID);
		List<ConsignmentVO> cVOList = new ArrayList<>();
		for (Consignment c : clist) {
			ConsignmentVO cVO = new ConsignmentVO();
			cVO.setSource(c.getSource());
			cVO.setDestination(c.getDestination());
			cVO.setSortingAgent(c.getSortingAgent());
			cVO.setTrackingId(c.getTrackingId());
			cVO.setStatus(c.getStatus());
			cVOList.add(cVO);
		}

		return cVOList;
	}

	@Override
	public List<ConsignmentVO> getListOfIncomingConsignments(int sortingAgentId) {
		logger.info("Inside method : getListOfConsignments");
		SortingAgent a = sortingAgentRepo.findById(sortingAgentId);
		City city = a.getCity();
		List<Branch> branchList = branchRepo.findBranchByCityId(city.getId());
		logger.info("branch list size" + branchList.size());
		List<ConsignmentVO> conVoList = new ArrayList<ConsignmentVO>();
		List<Consignment> list = new ArrayList<Consignment>();
		try {
			List<Status> sList = statusRepo.findByCodeAndIoStatus(a.getUser().getId(), IOStatus.INCOMING);
			for (Status s : sList) {
				int id = s.getId();
				list.add(consignmentRepo.findByStatusId(id));
			}
			logger.info("size of sList" + sList.size());
		} catch (Exception e) {
			logger.info("no incoming consignments as of now");
		}
		logger.info("size of list" + list.size());
		for (Consignment c : list) {
			logger.info("track is " + c.getTrackingId());
		}

		for (Branch b : branchList) {
			List<Consignment> conList = b.getConsignment();
			List<Consignment> filtered = conList.stream()
					.filter(ios -> ios.getStatus().getIoStatus() == IOStatus.OUTGOING
							&& ios.getStatus().getCode() == b.getUser().getId())
					.collect(Collectors.toList());
			logger.info("bRANCH " + b.getId());
			list.addAll(filtered);
		}
		logger.info("size of consignment" + list.size());

		for (Consignment c : list) {
			ConsignmentVO cvo = new ConsignmentVO();
			cvo.setTrackingId(c.getTrackingId());
			cvo.setModeOfDelivery(c.getDeliveryMode());
			cvo.setSource(c.getSource());
			cvo.setDestination(c.getDestination());
			cvo.setConsignmentCreatedAt(c.getConsignmentCreatedAt());
			cvo.setLastUpdateTime(c.getConsignmentUpdatedAt());
			cvo.setId(c.getId());
			conVoList.add(cvo);
		}
		logger.info(conVoList.size());
		return conVoList;
	}

	@Override
	public void receiveConsignments(String trackingId, int sortingAgentId) {
		logger.info("Entering receive : sa ");
		logger.info("Tracking Id " + trackingId);
		Consignment consignment = consignmentRepo.findByTrackingId(trackingId);
		SortingAgent sortingAgent = sortingAgentRepo.getById(sortingAgentId);
		consignment.setSortingAgent(sortingAgent);
		Status status = consignment.getStatus();
		status.setCode(sortingAgent.getUser().getId());
		status.setIoStatus(IOStatus.RECEIVED);
		consignment.setStatus(status);
		
		StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
		sTrackingDetails.setCode(status.getCode());
		sTrackingDetails.setIoStatus(status.getIoStatus());
		sTrackingDetails.setDescription("Consignment reached at source sorting agent. Will be shipped");
		statusTrackingDetailsRepo.save(sTrackingDetails);
		List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
		statustrackingdetailsList.add(sTrackingDetails);
		sTrackingDetails.setStatus(status);
		sTrackingDetails.setStatusIdFinal(status.getId());
		status.setStatusTrackingDetails(statustrackingdetailsList);
		
		logger.info("Leaving receive : sa");
		consignmentRepo.save(consignment);
	}

	@Override
	public List<Consignment> ListOfOutgoingConsignment(int sortingAgentUserId) { // send consignments
		logger.info("Inide outgoing method: ListOfOutgoingConsignment sortingAgent");
		List<Status> sList = statusRepo.findByCodeAndIoStatus(sortingAgentUserId, IOStatus.RECEIVED);
		List<Consignment> outgoingList = new ArrayList<Consignment>();
		for (Status st : sList) {
			int sId = st.getId();
			Consignment consignment = consignmentRepo.findByStatus(sId);
			outgoingList.add(consignment);
		}
		logger.info("Leaving outgoung method:ListOfOutgoingConsignment at sortingAgent");
		return outgoingList;
	}

	public void sendConsignment(String trackingId, int sortingAgentId) {
		logger.info("Inside method:sendConsignment at sortingAgent >>");
		List<DeliveryPerson> deliveryPersons = deliveryPersonRepo.findAllDeliveryPersonBySortingAgentId(sortingAgentId);
		Consignment c = consignmentRepo.findByTrackingId(trackingId);
		SortingAgent sortingAgent = sortingAgentRepo.findById(sortingAgentId);
		c.setSortingAgent(sortingAgent);
		logger.info("size of delivery person" + deliveryPersons.size());
		Status sList = statusRepo.findIdByCodeAndIoStatus(sortingAgent.getUser().getId(), IOStatus.RECEIVED);
		int sId = sList.getId();
		Consignment consignment = consignmentRepo.findByStatus(sId);
		if (consignment.getDestination().equalsIgnoreCase(sortingAgent.getCity().getCityName())) {
			// assign to delivery person
			int dId = deliveryPersonService.assignConsignment(consignment, deliveryPersons,
					sortingAgent.getUser().getId());
			DeliveryPerson dPerson = deliveryPersonRepo.findById(dId);
			StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
			Status status = new Status();
			status = consignment.getStatus();
			status.setCode(dPerson.getUser().getId());
			status.setIoStatus(IOStatus.OUTGOING);
			consignment.setStatus(status);
			consignment.setDeliveryPerson(dPerson);
			consignmentRepo.save(consignment);
			statusRepo.save(status);
			
			sTrackingDetails.setStatusIdFinal(status.getId());
			sTrackingDetails.setCode(status.getCode());
			sTrackingDetails.setIoStatus(status.getIoStatus());
			sTrackingDetails.setDescription("Consignment with delivery person. Will be shipped");
			statusTrackingDetailsRepo.save(sTrackingDetails);
			List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
			statustrackingdetailsList.add(sTrackingDetails);
			sTrackingDetails.setStatus(status);
			status.setStatusTrackingDetails(statustrackingdetailsList);
		} else {
			if (consignment.getDeliveryMode().equalsIgnoreCase("airways")) {
				// send to same city airhub
				System.out.println("assigning to airhub ");
				AirHub airHub = airHubRepo.findByCity(sortingAgent.getCity());
				int airHubUserId = airHub.getUser().getId();
				Status status = consignment.getStatus();
				StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
				status.setCode(airHubUserId);
				status.setIoStatus(IOStatus.INCOMING);
				consignment.setStatus(status);
				consignmentRepo.save(consignment);
				statusRepo.save(status);
				sTrackingDetails.setStatusIdFinal(status.getId());
				sTrackingDetails.setCode(status.getCode());
				sTrackingDetails.setIoStatus(status.getIoStatus());
				sTrackingDetails.setDescription("Consignmnet reached at source airhub. Will be shipped");
				statusTrackingDetailsRepo.save(sTrackingDetails);
				List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
				statustrackingdetailsList.add(sTrackingDetails);
				sTrackingDetails.setStatus(status);
				status.setStatusTrackingDetails(statustrackingdetailsList);
				
			} else {
				System.out.println("assigning to destination city sorting agent");
				// send consignment to destination city sorting hub
				City city = cityRepo.findByCityName(consignment.getDestination());
				SortingAgent destSortingAgent = sortingAgentRepo.findByCity(city);// destination
				int destSortingAgentUserId = destSortingAgent.getUser().getId();
				Status status = consignment.getStatus();
				status.setCode(destSortingAgentUserId);
				status.setIoStatus(IOStatus.INCOMING);
				consignment.setStatus(status);
				consignmentRepo.save(consignment);
				statusRepo.save(status);
				
				StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
				sTrackingDetails.setCode(status.getCode());
				sTrackingDetails.setIoStatus(status.getIoStatus());
				sTrackingDetails.setDescription("Consignmnet reached at destination sorting agent. Will be shipped");
				statusTrackingDetailsRepo.save(sTrackingDetails);
				List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
				statustrackingdetailsList.add(sTrackingDetails);
				sTrackingDetails.setStatus(status);
				sTrackingDetails.setStatusIdFinal(status.getId());
				status.setStatusTrackingDetails(statustrackingdetailsList);

			}
		}
	}

	@Override
	public void addDeliveryPerson(int createdBy, String password, String email, String name) {

		String role = "Delivery Person";
		try {
			User user = saveAndGetUser(email, createdBy, password, role);
			DeliveryPerson dp = new DeliveryPerson();
			logger.info("Email " + email);
			logger.info("Name " + name);
			dp.setDeliveryPersonCreatedBy(createdBy);
			dp.setDeliveryPersonUpdatedBy(createdBy);
			dp.setUser(user);
			dp.setDeliveryPersonName(name);
			SortingAgent sa = sortingAgentRepo.findByUserId(createdBy);
			dp.setSortingAgent(sa);
			deliveryPersonRepo.save(dp);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public User saveAndGetUser(String email, int createdBy, String password, String roleCode) {
		// user and user role generation
		User user = new User();
		UserRole userRole = new UserRole();
		logger.info("User ===== " + user + "\n User role ===== " + userRole);
		logger.info("Role ===== " + roleCode);
		int roleDeliveryPersonId = roleRepo.findByCode(roleCode).getId();
		user.setPassword(password);
		user.setEmail(email);
		user.setUserCreatedBy(createdBy);
		user.setIsActive(Active.YES);
		User temp = userRepo.save(user);
		Role role = roleRepo.getById(roleDeliveryPersonId);
		// assign role and user to user role
		userRole.setUser(user);
		userRole.setRole(role);
		userRole.setUserRoleCreaedBy(createdBy);
		userRole.setIsActive(Active.YES);
		userRoleRepo.save(userRole);
		return temp;
	}

}