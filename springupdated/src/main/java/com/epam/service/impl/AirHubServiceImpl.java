package com.epam.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.entity.AirHub;
import com.epam.entity.City;
import com.epam.entity.Consignment;
import com.epam.entity.IOStatus;
import com.epam.entity.SortingAgent;
import com.epam.entity.Status;
import com.epam.entity.StatusTrackingDetails;
import com.epam.repo.AirHubRepo;
import com.epam.repo.CityRepo;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.SortingAgentRepo;
import com.epam.repo.StatusRepo;
import com.epam.repo.StatusTrackingDetailsRepo;
import com.epam.service.AirHubService;
import com.epam.vo.ConsignmentVO;

@Service
public class AirHubServiceImpl implements AirHubService {
	private static final Logger logger = LogManager.getLogger(AirHubServiceImpl.class);

	@Autowired
	StatusRepo statusRepo;

	@Autowired
	CityRepo cityRepo;

	@Autowired
	AirHubRepo airHubRepo;

	@Autowired
	ConsignmentRepo consignmentRepo;

	@Autowired
	SortingAgentRepo sortingAgentRepo;
	
	@Autowired
	StatusTrackingDetailsRepo statusTrackingDetailsRepo;

	@Override
	public List<ConsignmentVO> getIncomingConsignmentList(int airHubId) {
		logger.info("Inside method:getIncomingConsignmentList list  at airhub");
		logger.info("Airhub Id :" + airHubId);
		AirHub a = airHubRepo.findById(airHubId);
		logger.info("Coming here");

		City city = a.getCity();
		logger.info("City name " + city.getCityName());

		List<ConsignmentVO> conVoList = new ArrayList<ConsignmentVO>();

		List<Consignment> consignmentList = new ArrayList<Consignment>();

		try {
			logger.info("Airhub user id " + a.getUser().getId());
			List<Status> sList = statusRepo.findByCodeAndIoStatus(a.getUser().getId(), IOStatus.INCOMING);
			for (Status s : sList) {
				int id = s.getId();
				consignmentList.add(consignmentRepo.findByStatusId(id));
			}

			logger.info("size of sList" + sList.size());
		} catch (Exception e) {
			logger.info("no incoming consignments as of now");
		}
		for (Consignment c : consignmentList) {
			logger.info("track is " + c.getTrackingId());
		}

		logger.info("size of consignment" + consignmentList.size());

		for (Consignment c : consignmentList) {

			ConsignmentVO cvo = new ConsignmentVO();

			cvo.setTrackingId(c.getTrackingId());
			cvo.setModeOfDelivery(c.getDeliveryMode());
			cvo.setSource(c.getSource());
			cvo.setDestination(c.getDestination());
			cvo.setConsignmentCreatedAt(c.getConsignmentCreatedAt());
			cvo.setLastUpdateTime(c.getConsignmentUpdatedAt());
			cvo.setId(c.getId());

			conVoList.add(cvo);
		}

		logger.info("Leaving method:getIncomingConsignmentList at airhut");
		return conVoList;
	}

	@Override
	public void receiveConsignment(String trackingId, int airHubId) {

		logger.info("Inside method:receiveConsignments at airhut");
		Consignment c = consignmentRepo.findByTrackingId(trackingId);
		AirHub s = airHubRepo.findById(airHubId);
		c.setAirHub(s);
		Status status = c.getStatus();
		status.setCode(s.getUser().getId());
		status.setIoStatus(IOStatus.RECEIVED);
		c.setStatus(status);
		
		StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
		sTrackingDetails.setCode(status.getCode());
		sTrackingDetails.setIoStatus(status.getIoStatus());
		sTrackingDetails.setDescription("Consignmnet reached at source airhub. Will be shipped");
		statusTrackingDetailsRepo.save(sTrackingDetails);
		List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
		statustrackingdetailsList.add(sTrackingDetails);
		sTrackingDetails.setStatus(status);
		sTrackingDetails.setStatusIdFinal(status.getId());
		status.setStatusTrackingDetails(statustrackingdetailsList);
		logger.info("Leaving method:receiveConsignments at airhub");
		consignmentRepo.save(c);
	}

	@Override
	public List<Consignment> ListOfOutgoingConsignment(int id) { // need modification (send consignments)
		logger.info("Inide outgoing method: ListOfOutgoingConsignment airhub");
		AirHub airHub = airHubRepo.findById(id);
		List<Status> sList = statusRepo.findByCodeAndIoStatus(airHub.getUser().getId(), IOStatus.RECEIVED);
		List<Consignment> outgoingList = new ArrayList<Consignment>();
		for (Status st : sList) {
			int sId = st.getId();
			Consignment consignment = consignmentRepo.findByStatus(sId);
			outgoingList.add(consignment);
		}

		logger.info("Leaving outgoung method:ListOfOutgoingConsignment at airhub");
		return outgoingList;
	}

	public void sendConsignment(String trackingId, int airHubId) {
		logger.info("Inside method:sendConsignment at airhub >>");
		Consignment c = consignmentRepo.findByTrackingId(trackingId);
		AirHub airHub = airHubRepo.findById(airHubId);
		c.setAirHub(airHub);
		logger.info(airHub.getUser().getId());
		Status sList = statusRepo.findIdByCodeAndIoStatus(airHub.getUser().getId(), IOStatus.RECEIVED);
		if (sList == null)
			logger.info("iam NULL");
		List<Consignment> resList = new ArrayList<Consignment>();

		int sId = sList.getId();
		Consignment consignment = consignmentRepo.findByStatus(sId);
		resList.add(consignment);

		City city = cityRepo.findByCityName(consignment.getDestination());

		logger.info("City " + city.getCityName() + " Airhub City " + airHub.getCity().getCityName());
		if (airHub.getCity().getCityName().equals(city.getCityName())) {
			// send it to sorting hub agent
			SortingAgent sortingAgent = sortingAgentRepo.findByCity(city);
			int sortingAgentId = sortingAgent.getUser().getId();
			StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
			Status status = consignment.getStatus();
			logger.info("User Id " + sortingAgentId);
			status.setCode(sortingAgentId);
			status.setIoStatus(IOStatus.INCOMING);
			consignment.setStatus(status);
			consignmentRepo.save(consignment);
			statusRepo.save(status);
			
			
			sTrackingDetails.setStatusIdFinal(status.getId());
			sTrackingDetails.setCode(status.getCode());
			sTrackingDetails.setIoStatus(status.getIoStatus());
			sTrackingDetails.setDescription("Consignmnet with destination sorting agent. Will be shipped");
			statusTrackingDetailsRepo.save(sTrackingDetails);
			List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
			statustrackingdetailsList.add(sTrackingDetails);
			sTrackingDetails.setStatus(status);
			status.setStatusTrackingDetails(statustrackingdetailsList);
		} else // another airhub
		{
			AirHub destAirHub = airHubRepo.findByCity(city);// destination
			int	destAirHubId = destAirHub.getUser().getId();
			StatusTrackingDetails sTrackingDetails = new StatusTrackingDetails();
			Status status = consignment.getStatus();
			status.setCode(destAirHubId);
			status.setIoStatus(IOStatus.INCOMING);
			consignment.setStatus(status);
			consignmentRepo.save(consignment);
			statusRepo.save(status);
			
			sTrackingDetails.setStatusIdFinal(status.getId());
			sTrackingDetails.setCode(status.getCode());
			sTrackingDetails.setIoStatus(status.getIoStatus());
			sTrackingDetails.setDescription("Consignmnet with destination airhub. Will be shipped");
			statusTrackingDetailsRepo.save(sTrackingDetails);
			List<StatusTrackingDetails> statustrackingdetailsList = new ArrayList<StatusTrackingDetails>();
			statustrackingdetailsList.add(sTrackingDetails);
			sTrackingDetails.setStatus(status);
			status.setStatusTrackingDetails(statustrackingdetailsList);
			
		}

	}

	@Override
	public int findByUserId(int airHubUserId) {
		return airHubRepo.findByUserId(airHubUserId).getId();
	}
}
