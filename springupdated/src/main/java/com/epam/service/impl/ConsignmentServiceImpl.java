package com.epam.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.entity.City;
import com.epam.entity.Consignment;
import com.epam.entity.StatusTrackingDetails;
import com.epam.repo.AirHubRepo;
import com.epam.repo.BranchRepo;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.CustomerRepo;
import com.epam.repo.DeliveryPersonRepo;
import com.epam.repo.RoleRepo;
import com.epam.repo.SortingAgentRepo;
import com.epam.repo.StatusTrackingDetailsRepo;
import com.epam.repo.UserRepo;
import com.epam.repo.UserRoleRepo;
import com.epam.service.ConsignmentService;
import com.epam.vo.ConsignmentVO;

@Service
public class ConsignmentServiceImpl implements ConsignmentService {
	private static final Logger logger = LogManager.getLogger(ConsignmentServiceImpl.class);
	@Autowired
	ConsignmentRepo consignmentRepo;
	@Autowired
	UserRoleRepo userRoleRepo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	SortingAgentRepo sortingAgentRepo;
	
	@Autowired
	DeliveryPersonRepo deliveryPersonRepo;
	
	@Autowired
	AirHubRepo airHubRepo;
	
	@Autowired
	BranchRepo branchRepo;
	
	@Autowired
	CustomerRepo customerRepo;
	
	@Autowired
	RoleRepo roleRepo;
	
	@Autowired
	StatusTrackingDetailsRepo statusTrackingDetailsRepo;

	@Override
	public String findTrackingId(String string) {
		return consignmentRepo.findTrackingId(string);
	}

	@Override
	public ConsignmentVO trackConsignment(String trackId) {

		try {
			// fetch consignment
			Consignment c = consignmentRepo.findByTrackingId(trackId);

			logger.info("Id " + c.getId() + " Tracking Id " + c.getTrackingId());
			ConsignmentVO cvo = new ConsignmentVO();
			cvo.setStatus(c.getStatus());
			cvo.setTrackingId(trackId);
			cvo.setModeOfDelivery(c.getDeliveryMode());
			cvo.setSource(c.getSource());
			cvo.setDestination(c.getDestination());
			cvo.setConsignmentCreatedAt(c.getConsignmentCreatedAt());
			cvo.setLastUpdateTime(c.getConsignmentUpdatedAt());
			return cvo;

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
			return null;
		}

	}

	@Override
	public List<ConsignmentVO> trackConsignmentAllStatus(String trackId) {
		Consignment c = consignmentRepo.findByTrackingId(trackId);
		int statusId = c.getStatus().getId();
		List<StatusTrackingDetails> sDetails = statusTrackingDetailsRepo.getByStatusId(statusId);
		System.out.println("sdetails size" + sDetails.size());
		List<ConsignmentVO> consresList = new ArrayList<ConsignmentVO>();
		int sortingAgentCode = 0;
		for (StatusTrackingDetails sTrackingDetails : sDetails) {
			try {
				ConsignmentVO cvo = new ConsignmentVO();
				cvo.setIoStatus(sTrackingDetails.getIoStatus());
				cvo.setDescription(sTrackingDetails.getDescription());
				cvo.setConsignmentUpdatedAt(sTrackingDetails.getConsignmentUpdatedAt());
				int userId = sTrackingDetails.getCode();
				// code in status table
				int roleId = userRoleRepo.findRoleIdByUserId(userId);
				logger.info("User id " + userId); // 1 5 (2 3 4 6)
				if (roleId == 5) {
					cvo.setCurrentEntity("Customer");
					City customerCity = sortingAgentRepo.findByUserId(sortingAgentCode).getCity();
					cvo.setCity(customerCity);
				} else {
					logger.info("Role id " + roleId);
					if (roleId == 2) // branch
					{
						System.out.println("branch" + roleId);
						cvo.setCurrentEntity("Branch");
						Integer code = sTrackingDetails.getCode();
						City branchCity = branchRepo.findByUserId(code).getCity();
						cvo.setCity(branchCity);
					} else if (roleId == 4) // airhub
					{
						System.out.println("arihub" + roleId);
						cvo.setCurrentEntity("AirHub");
						Integer code = sTrackingDetails.getCode();
						City airHubCity = airHubRepo.findByUserId(code).getCity();
						cvo.setCity(airHubCity);
					} else if (roleId == 3) // sorting hub
					{
						System.out.println("sortingAgent" + roleId);
						cvo.setCurrentEntity("Sorting Agent");
						Integer code = sTrackingDetails.getCode();
						sortingAgentCode = code;
						City sortingAgentCity = sortingAgentRepo.findByUserId(code).getCity();
						cvo.setCity(sortingAgentCity);
					} else if (roleId == 6) {
						cvo.setCurrentEntity("Delivery Person");
						City deliveryGuyCity = sortingAgentRepo.findByUserId(sortingAgentCode).getCity();
						cvo.setCity(deliveryGuyCity);
					}
				}
				System.out.println(cvo.getCurrentEntity());
				consresList.add(cvo);
			} catch (Exception e) {
				e.printStackTrace();
				logger.info(e);
				return null;
			}
		}
		return consresList;
	}

}
