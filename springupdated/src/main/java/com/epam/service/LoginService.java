package com.epam.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.epam.entity.Admin;
import com.epam.entity.Branch;
import com.epam.entity.Customer;
import com.epam.entity.DeliveryPerson;
import com.epam.entity.User;
import com.epam.entity.UserRole;

@Service
public interface LoginService {
	public boolean updatePassword(String mail, String oldPassword, String newPassword);
	public boolean setPassword(int id,String password);
	public boolean saveUser(String email, String password, String userName);
	public List<Admin> getAdminDetails(int id);
	public List<Branch> getBranchDetails(int id);
	public List<Customer> getCustomerDetails(int id);
	public List<User> checkLogin(String email, String password);
	public List<UserRole> checkRole(int id);
	public List<DeliveryPerson> getDeliveryPersonDetails(int id);
	public User checkPassword(String email);
}
