package com.epam.service;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface BranchService {
	public List<String> generateReport(int month, int year, int branchId);

	public String setDetails(String name, String email, String source, String destination, String modeofdelivery,
			int userId, String sourceAddress, String destinationAddress, int sourceZipCode, int destinationZipCode,
			double weight, double price);

	public double calculatePrice(double weight);
}
