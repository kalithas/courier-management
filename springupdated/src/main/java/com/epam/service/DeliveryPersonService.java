package com.epam.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.entity.Consignment;
import com.epam.entity.DeliveryPerson;
import com.epam.vo.ConsignmentVO;


@Service
public interface DeliveryPersonService{
	public void deliverConsignment(int consignmentId);
	public int assignConsignment(Consignment consignment,List<DeliveryPerson> dList,int sortingAgentId) ;
	public List<ConsignmentVO> getConsignmentList(int deliveryPersonId);

}
