package com.epam.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.epam.entity.*;
@Service
public interface CityService {
	public List<City> getCities();
}
