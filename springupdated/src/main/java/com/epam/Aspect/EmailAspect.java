package com.epam.Aspect;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.epam.entity.Consignment;
import com.epam.entity.MailRequest;
import com.epam.repo.ConsignmentRepo;
import com.epam.repo.UserRepo;
import com.epam.service.ConsignmentService;
import com.epam.service.EmailService;

@Aspect
@Component
public class EmailAspect {

	private static final Logger logger = LogManager.getLogger(EmailAspect.class);
	
	@Autowired
	ConsignmentRepo consignmentRepo;
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	ConsignmentService consignmentService;


	@After("execution(* com.epam.service.impl.BranchServiceImpl.setDetails(..))")
	public void sendEmailAfterOrder(JoinPoint point) {
		int emailType = 1;
		Object[] userDetails = point.getArgs();
		Map<String, Object> model = new HashMap<>();
		model.put("Name", userDetails[0].toString());
		model.put("Location", "Bangalore,India");
		MailRequest request = new MailRequest();
		request.setFrom("donotreplytoprodelivery@gmail.com");
		request.setName(userDetails[0].toString());
		request.setSubject("Your package confirmation mail");
		request.setTo(userDetails[1].toString());
		String trackIdString = consignmentService.findTrackingId(userDetails[1].toString());
		logger.info("Received tracking id" + trackIdString);
		model.put("consignmentId", trackIdString);
		logger.info(
				"email in aspect" + userDetails[0].toString() + userDetails[1].toString() + userDetails[2].toString());
		emailService.sendEmail(request, model, emailType);
		logger.info("This aspect got executed !" + point.getSignature().getName());
	}
	
	@After("execution(* com.epam.service.impl.DeliveryPersonServiceImpl.deliverConsignment(..))")
	public void sendEmailAfterDelivery(JoinPoint point) {
		int emailType = 3;
		Object[] userDetails = point.getArgs();
		int consignmentId = (int)userDetails[0];
		System.out.println("consignment id "+ consignmentId);
		Consignment consignment = consignmentRepo.findById(consignmentId);
		String customerName = consignment.getCustomer().getCustomerName();
		String customerEmail = consignment.getCustomer().getUser().getEmail();
		System.out.println("email to sent "+customerEmail);
		Map<String, Object> model = new HashMap<>();
		model.put("Name", customerName);
		model.put("Location", "Bangalore,India");
		MailRequest request = new MailRequest();
		request.setFrom("donotreplytoprodelivery@gmail.com");
		request.setName(userDetails[0].toString());
		request.setSubject("Your package got delivered now!!");
		request.setTo(customerEmail);
		emailService.sendEmail(request, model, emailType);
		System.out.println("end of email aspect");
	}

}