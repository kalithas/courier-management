package com.epam.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.entity.Cookie;
import com.epam.repo.CookieRepo;

@Component
public class CookieUtil {
	
	@Autowired
	CookieRepo cookieRepo;
	
	public  int getUserId()
	{
		return cookieRepo.findById(1).getUserId();
	}
	
	public void saveCookie(Cookie cookie)
	{
		cookieRepo.save(cookie);
	}
	
	
}
