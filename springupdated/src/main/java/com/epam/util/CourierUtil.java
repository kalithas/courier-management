package com.epam.util;

import java.util.HashMap;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CourierUtil
{
	private static final Logger logger = LogManager.getLogger(CourierUtil.class);
	private static HashMap<Integer,String> otps = new HashMap<Integer,String>();
	private static Random r = new Random();
	
	public static String getPassword() {
		String password = "";
		
		
		for(int i=0;i<15;i++)
		{
			int v = r.nextInt(26);
			password += (char)('a'+v);
		}
		 
		return password;
	}
	
	public static String sendOtp(int userId)
	{
		String s = "";
		
		for(int i=0;i<6;i++)
		{
			s+=r.nextInt(9);
		}
		
		otps.put(userId, s);
		
		 logger.info(s);
		return s;
		
//		return 1;
	}
	
	public static boolean verifyOtp(int id,String userOtp)
	{
		 logger.info("Otp in backend "+otps.get(id)+"User Otp "+userOtp);
		return otps.get(id).equals(userOtp);
	}
}