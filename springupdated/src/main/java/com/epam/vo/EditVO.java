package com.epam.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditVO {
	
	String email;
	String oldPassword;
	String newPassword;
}