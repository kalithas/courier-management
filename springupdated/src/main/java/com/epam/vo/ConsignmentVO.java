package com.epam.vo;



import java.util.Date;
import com.epam.entity.City;
import com.epam.entity.IOStatus;
import com.epam.entity.SortingAgent;
import com.epam.entity.Status;
import com.epam.entity.StatusTrackingDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsignmentVO {



private int id;
private Status status;
private String trackingId;
private String modeOfDelivery; //modification needed
private String source;
private String destination;
private Date consignmentCreatedAt;
private Date consignmentUpdatedAt;
private Date lastUpdateTime;
private String currentEntity;
private City city;
private SortingAgent sortingAgent;
private StatusTrackingDetails statusTrackingDetails;
private IOStatus ioStatus;
private String description;
}