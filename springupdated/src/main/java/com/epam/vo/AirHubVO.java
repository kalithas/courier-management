package com.epam.vo;

import java.util.List;

import com.epam.entity.Consignment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AirHubVO {
	private String mail;
	private String city;
	private int sourceAirHubId;
	private int destAirHubId;
	private List<Consignment> consignmentList;
}