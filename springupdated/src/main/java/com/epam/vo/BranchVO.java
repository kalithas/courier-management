package com.epam.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BranchVO {

	private String mail;
	private String address;
	private String zipCode;
	private String city;
	
}
