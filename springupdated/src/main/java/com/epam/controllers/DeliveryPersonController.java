package com.epam.controllers;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.repo.DeliveryPersonRepo;
import com.epam.service.DeliveryPersonService;
import com.epam.util.CookieUtil;
import com.epam.vo.ConsignmentVO;

@Controller
@RequestMapping("/DPC")
public class DeliveryPersonController {
	private static final Logger logger = LogManager.getLogger(DeliveryPersonController.class);
	
	private int deliveryPersonId;
	private int deliveryPersonUserId;
	
	@Autowired
	DeliveryPersonService deliveryPersonService;
	
	@Autowired
	DeliveryPersonRepo deliveryPersonRepo;
	
	@Autowired
	CookieUtil cookieUtil;
	


	@RequestMapping("/update/{consignmentId}")
	public String deliverConsignment(@PathVariable int consignmentId) {
		 logger.info("entered controller");
		deliveryPersonService.deliverConsignment(consignmentId);

		return "redirect:/DPC/list";
	}

	@RequestMapping("/list")
	public ModelAndView listConsignments() {
		
		deliveryPersonUserId = cookieUtil.getUserId();
		deliveryPersonId = deliveryPersonRepo.findByUserId(deliveryPersonUserId).getId();
		ModelAndView mView = new ModelAndView("DeliveryPerson");
		List<ConsignmentVO> lvo = deliveryPersonService.getConsignmentList(deliveryPersonId);
		mView.addObject("list", lvo);
		return mView;
	}

}
