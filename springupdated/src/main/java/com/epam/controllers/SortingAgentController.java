package com.epam.controllers;

import java.util.List;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.epam.entity.Consignment;
import com.epam.repo.SortingAgentRepo;
import com.epam.service.LoginService;
import com.epam.service.SortingAgentService;
import com.epam.util.CookieUtil;
import com.epam.util.CourierUtil;
import com.epam.vo.ConsignmentVO;
import com.epam.vo.EditVO;

@Controller
@RequestMapping("/SAC")
public class SortingAgentController {
	private static final Logger logger = LogManager.getLogger(SortingAgentController.class);
	
	private int sortingAgentId;
	private int sortingAgentUserId;
	private int cityId;
	
	@Autowired 
	SortingAgentService sortingAgentService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	SortingAgentRepo sortingAgentRepo;

	@Autowired
	CookieUtil cookieUtil;


	@RequestMapping("/getBySource")
	public ModelAndView getAllConsignments() {
		sortingAgentUserId = cookieUtil.getUserId();
		sortingAgentId = sortingAgentRepo.findByUserId(sortingAgentUserId).getId();
		cityId = sortingAgentRepo.findByUserId(sortingAgentUserId).getCity().getId();
		List<ConsignmentVO> clist = sortingAgentService.findByCity(cityId);
		ModelAndView mView = new ModelAndView("ViewConsignmentHistory");
		mView.addObject("history", clist);
		return mView;
	}

	@RequestMapping("/incoming")
	public ModelAndView incoming() {
		sortingAgentUserId = cookieUtil.getUserId();
		sortingAgentId = sortingAgentRepo.findByUserId(sortingAgentUserId).getId();
		cityId = sortingAgentRepo.findByUserId(sortingAgentUserId).getCity().getId();
		List<ConsignmentVO> conVoList = sortingAgentService.getListOfIncomingConsignments(sortingAgentId);
		conVoList.forEach(System.out::println);
		ModelAndView mav = new ModelAndView("IncomingProcessShipment");
		mav.addObject("conVO", conVoList);
		return mav;
	}
	
	@RequestMapping("/outgoing")
	public ModelAndView receiveOutgoingconsignmnet() {
		sortingAgentUserId = cookieUtil.getUserId();
		List<Consignment> res = sortingAgentService.ListOfOutgoingConsignment(sortingAgentUserId);
		 logger.info(res.size());
		ModelAndView mav = new ModelAndView("OutgoingProcessShipment");
		mav.addObject("conVO", res);
		return mav;
	}

	@GetMapping("/receive/{trackingId}")
	public String outgoing(@PathVariable String trackingId) {
		 logger.info("Receive consignmnets going...................");
		 logger.info("TrackingId "+trackingId);
		 sortingAgentUserId = cookieUtil.getUserId();
			sortingAgentId = sortingAgentRepo.findByUserId(sortingAgentUserId).getId();
		sortingAgentService.receiveConsignments(trackingId, sortingAgentId);
		return "redirect:/SAC/incoming";
	}

	@GetMapping("/outgoing/{trackingId}")
	public String outgoingConsignments(@PathVariable String trackingId) {
		sortingAgentUserId = cookieUtil.getUserId();
		System.out.println("saaaa"+sortingAgentUserId);
		sortingAgentId = sortingAgentRepo.findByUserId(sortingAgentUserId).getId();
		 logger.info("Receive consignmnets going...................");
		 logger.info("TrackingId "+trackingId);
		sortingAgentService.sendConsignment(trackingId, sortingAgentId);
		return "redirect:/SAC/outgoing";

	}
	
	@RequestMapping("/addDeliveryPerson")
	public ModelAndView addDeliveryPerson() {
		ModelAndView mav = new ModelAndView("AddDeliveryPerson");
		return mav;
	}

	@PostMapping("/addDeliveryPerson")
	public ModelAndView addDeliveryPerson1(@RequestParam("name") String name, @RequestParam("email") String email) {
		ModelAndView mav = new ModelAndView("redirect:/SAC/addDeliveryPerson");
		sortingAgentUserId = cookieUtil.getUserId();
		sortingAgentId = sortingAgentRepo.findByUserId(sortingAgentUserId).getId();
		cityId = sortingAgentRepo.findByUserId(sortingAgentUserId).getCity().getId();
		sortingAgentService.addDeliveryPerson(sortingAgentUserId,CourierUtil.getPassword(),email,name);
		return mav;
	}

	@GetMapping("/editProfile")
	public ModelAndView editProfile() {
		ModelAndView mView = new ModelAndView("EditSortingHubProfile");

		return mView;
	}

	@PostMapping("/editProfile")
	public ModelAndView editProfile1(EditVO editVO) {
		String email = editVO.getEmail();
		String oldPassword = editVO.getOldPassword();
		String newPassword = editVO.getNewPassword();
		if (oldPassword != null && newPassword != null && loginService.updatePassword(email, oldPassword, newPassword)) {
			 logger.info("Updated succesfully...");
			ModelAndView mView = new ModelAndView("redirect:/SAC/editProfile");
			return mView; 
		} else {
			 logger.info("Wrong password...");
		}
		ModelAndView mView = new ModelAndView("error");
		return mView;
	}
	
	@RequestMapping("/SACHome")
	public ModelAndView homePage() {
		ModelAndView mView = new ModelAndView("SortingHubPage");
		return mView;
	}

}
