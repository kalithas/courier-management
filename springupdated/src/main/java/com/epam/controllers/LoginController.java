package com.epam.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.epam.entity.Admin;
import com.epam.entity.Branch;
import com.epam.entity.Cookie;
import com.epam.entity.Customer;
import com.epam.entity.DeliveryPerson;
import com.epam.entity.User;
import com.epam.entity.UserRole;
import com.epam.service.AdminService;
import com.epam.service.DeliveryPersonService;
import com.epam.service.LoginService;
import com.epam.util.CookieUtil;
import com.epam.vo.ConsignmentVO;

@RestController
@RequestMapping("/signin")
public class LoginController {
	private static final Logger logger = LogManager.getLogger(LoginController.class);

	@Autowired
	LoginService loginService;

	@Autowired
	CookieUtil cookieUtil;
	
	@Autowired
	AdminService adminService;


	
	@Autowired
	DeliveryPersonService deliveryPersonService;
	
	@RequestMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("index");

		return mav;
	}

	@GetMapping("/login")
	public ModelAndView loginBlock() {

		ModelAndView mav = new ModelAndView("Login");
		User user = new User();
		Customer customer = new Customer();
		Admin admin = new Admin();
		Branch branch = new Branch();
		DeliveryPerson deliver = new DeliveryPerson();
		mav.addObject("user", user);
		mav.addObject("customer", customer);
		mav.addObject("admin", admin);
		mav.addObject("branch", branch);
		mav.addObject("deliveryagent", deliver);
		return mav;
	}
	@PostMapping("/login")
	public ModelAndView checkLogin(@ModelAttribute("user") User user, @ModelAttribute("customer") User customer,
			@ModelAttribute("admin") Admin admin, HttpSession session,HttpServletRequest request, HttpServletResponse response) {
		
		List<User> uList = loginService.checkLogin(user.getEmail(), user.getPassword());
		System.out.println(uList);
		if (uList.size() > 0) {
			int id = uList.get(0).getId();
			Cookie cookie = new Cookie(1,id); // id = userId
			cookieUtil.saveCookie(cookie);
			 
			 logger.info(id);
			List<UserRole> ur = loginService.checkRole(id);
			if (ur.get(0).getRole().getId() == 5) {
				List<Customer> customers = loginService.getCustomerDetails(id);
				Customer customer2 = customers.get(0);
				ModelAndView mav = new ModelAndView("CustomerPage");
				mav.addObject("customer", customer2);
				return mav;  
			}

			if (ur.get(0).getRole().getId() == 2) {
				List<Branch> branchs = loginService.getBranchDetails(id);
				Branch branch1 = branchs.get(0);
				ModelAndView mav = new ModelAndView("BranchEmployee");
				mav.addObject("branch", branch1);
				 logger.info("branch");
				return mav;
			}

			if (ur.get(0).getRole().getId() == 1) {
				ModelAndView mav = new ModelAndView("AdminHome");				return mav;
			}
			if (ur.get(0).getRole().getId() == 6) {
				List<DeliveryPerson> deliveryPersons = loginService.getDeliveryPersonDetails(id);
				DeliveryPerson deliveryPerson = deliveryPersons.get(0);
				ModelAndView mav = new ModelAndView("DeliveryPerson");
				List<ConsignmentVO> lvo = deliveryPersonService.getConsignmentList(deliveryPerson.getId());
				mav.addObject("list", lvo);
				return mav;
			}
			if (ur.get(0).getRole().getId() == 3) {
				ModelAndView mav = new ModelAndView("SortingHubPage");
				return mav;
			}

			if (ur.get(0).getRole().getId() == 4) {
				ModelAndView mav = new ModelAndView("AirHubPage");
				return mav;
			}

		}
		ModelAndView modelAndView = new ModelAndView("error");
		return modelAndView;

	}

	@GetMapping("/forgotPassword")
	public ModelAndView changePassword() {
		ModelAndView mav = new ModelAndView("ForgotPassword");
		return mav;
	}

	@PostMapping("/forgotPassword")
	public ModelAndView checkPassword(@RequestParam("email") String email, @RequestParam("password") String password,
			@RequestParam("checkPassword") String checkPassword) {
		boolean res = false;
		if (password.equals(checkPassword)) {
			User user = loginService.checkPassword(email);
			int id = user.getId();
			res = loginService.setPassword(id, password);

		}
		if (res == true) {
			ModelAndView modelAndView = new ModelAndView("Login");
			User user = new User();
			modelAndView.addObject("user", user);
			return modelAndView;

		}
		ModelAndView modelAndView1 = new ModelAndView("ForgotPassword");
		return modelAndView1;

	}

	

}
