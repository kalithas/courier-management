package com.epam.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.entity.User;
import com.epam.repo.CustomerRepo;
import com.epam.repo.UserRepo;
import com.epam.service.ConsignmentService;
import com.epam.service.CustomerService;
import com.epam.service.LoginService;
import com.epam.util.CookieUtil;
import com.epam.vo.ConsignmentVO;
import com.epam.vo.EditVO;

@Controller
@RequestMapping("/CC")
public class CustomerController {
	private static final Logger logger = LogManager.getLogger(CustomerController.class);
	
	private  int customerId;
	private int customerUserId;
	
	
	@Autowired
	CustomerService customerService;

	@Autowired
	ConsignmentService consignmentService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	CustomerRepo customerRepo;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	CookieUtil cookieUtil;
	


	@RequestMapping("/Home")
	public ModelAndView homePage() {
		ModelAndView mView = new ModelAndView("CustomerPage");
		return mView;
	}

	@RequestMapping("/trackConsignment")
	public ModelAndView trackConsginment() {
		ModelAndView mView = new ModelAndView("TrackConsignment");
		return mView;
	}

	/*@PostMapping("/trackConsignment")
	public ModelAndView trackConsignment1(@RequestParam("trackId") String trackId) {
		 logger.info("calling...");
		ConsignmentVO cvo = consignmentService.trackConsignment(trackId);
		ModelAndView mView = new ModelAndView("ConsignmentStatus");
		mView.addObject("st", cvo);
		return mView;
	}*/
	
	@PostMapping("/trackConsignment")
	public ModelAndView trackConsignment1(@RequestParam("trackId") String trackId) {
		logger.info("calling...");
		ConsignmentVO cvo = consignmentService.trackConsignment(trackId);
		System.out.println("entered trackConsignment controller");
		ModelAndView mView = new ModelAndView("ConsignmentStatusCustomer");
		mView.addObject("st", cvo);
		List<ConsignmentVO> reStatusTrackingDetails = consignmentService.trackConsignmentAllStatus(trackId);
		System.out.println("size of tracking details" + reStatusTrackingDetails.size());
		mView.addObject("allStatusReport", reStatusTrackingDetails);
		return mView;
	}

	@RequestMapping("/viewConsignmentHistory")
	public ModelAndView viewConsginmentHistory() {
		customerUserId = cookieUtil.getUserId();
		customerId = customerRepo.findByUserId(customerUserId).getId();
		ModelAndView mView = new ModelAndView("ViewConsignmentHistory");
		List<ConsignmentVO> list = customerService.getConsignmentList(customerId);
		mView.addObject("history", list);
		return mView;
	}
	

	@RequestMapping("/viewProfile")
	public ModelAndView viewProfile() {
		ModelAndView mView = new ModelAndView("ViewProfile");

		return mView;
	}
	
	@GetMapping("/editProfile")
	public ModelAndView editProfile() {
		ModelAndView mView = new ModelAndView("EditProfile");

		return mView;
	}

	@PostMapping("/editProfile")
	public ModelAndView editProfile1(EditVO editVO) {
		String email = editVO.getEmail();
		String oldPassword = editVO.getOldPassword();
		String newPassword = editVO.getNewPassword();
		if (oldPassword != null && newPassword != null && loginService.updatePassword(email, oldPassword, newPassword)) {
			 logger.info("Updated succesfully...");
			ModelAndView mView = new ModelAndView("redirect:/CC/editProfile");
			return mView;
		} else {
			 logger.info("Wrong password...");
		}
		ModelAndView mView = new ModelAndView("error");
		return mView;
	}
	
	@RequestMapping("/giveFeedback")
	public ModelAndView giveCustomerFeedback() {
		ModelAndView mView = new ModelAndView("CustomerFeedback");
		customerUserId = cookieUtil.getUserId();
		User user = userRepo.findById(customerUserId);
		String userEmail = user.getEmail();
		customerId = customerRepo.findByUserId(customerUserId).getId();
		List<ConsignmentVO> list = customerService.getConsignmentList(customerId);
		mView.addObject("consignments", list);
		mView.addObject("userEmail", userEmail);

		return mView;
	}

	@PostMapping("/giveFeedback")
	public ModelAndView giveFeedBack(@RequestParam("trackingId") String consignmentId,
			@RequestParam("email") String email, @RequestParam("description") String description) {
	
		String res = customerService.giveFeedback(consignmentId, email, description);
		 logger.info("consignemnt id in give feedbcak"+consignmentId);
		if (res.equalsIgnoreCase("Success")) {
			ModelAndView modelAndView = new ModelAndView("redirect:/CC/giveFeedback");
			return modelAndView;

		}
		ModelAndView modelAndView1 = new ModelAndView("FeedbackError");
		return modelAndView1;
	}

}
