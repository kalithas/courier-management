package com.epam.controllers;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.epam.repo.BranchRepo;
import com.epam.repo.UserRepo;
import com.epam.service.BranchService;
import com.epam.util.CookieUtil;

@Controller
@RequestMapping("/BC")
public class BranchController {
	private static final Logger logger = LogManager.getLogger(BranchController.class);

	private int branchUserId;

	@Autowired
	private BranchService branchService;

	@Autowired
	UserRepo userRepo;

	@Autowired
	BranchRepo branchRepo;

	@Autowired
	CookieUtil cookieUtil;

	@RequestMapping("/report")
	public String getConsignmentPage(Model model) {

		return "ViewBranchReports";
	}

	@PostMapping("/report")
	public String getBranchConsignments(@RequestParam("month") String month, @RequestParam("year") String year,
			Model model) {
		branchUserId = cookieUtil.getUserId();
		logger.info("generate report for the branch: " + " with month: " + month + " year: " + year);
		List<String> list = branchService.generateReport(Integer.parseInt(month), Integer.parseInt(year), branchUserId); // 2->
		logger.info("data " + list);
		model.addAttribute("report", list);
		return "ViewBranchReportData";

	}

	@GetMapping("/createConsignment")
	public ModelAndView createConsignment() {
		ModelAndView mav = new ModelAndView("AddConsignment");
		return mav;
	}

	@PostMapping("/createConsignment")
	public ModelAndView setConsignmentDetails(@RequestParam("name") String name, @RequestParam("email") String email,
			@RequestParam("source") String source, @RequestParam("destination") String destination,
			@RequestParam("modeofdelivery") String modeofdelivery, @RequestParam("sourceaddress") String sourceAddress,
			@RequestParam("destinationaddress") String destinationAddress,
			@RequestParam("sourcepin") String sourceZipCode, @RequestParam("destinationpin") String destinationZipCode,
			@RequestParam("weight") double weight, Model model) {

		branchUserId = cookieUtil.getUserId();

		double price = branchService.calculatePrice(weight);
		String res = branchService.setDetails(name, email, source, destination, modeofdelivery, branchUserId,
				sourceAddress, destinationAddress, Integer.parseInt(sourceZipCode),
				Integer.parseInt(destinationZipCode), weight, price);

		if (res.equalsIgnoreCase("success")) {
			ModelAndView mView = new ModelAndView("ConsignmentBill");
			mView.addObject("name", name);
			mView.addObject("source", source);
			mView.addObject("destination", destination);
			mView.addObject("weight", weight);
			mView.addObject("price", price);
			return mView;

		}
		ModelAndView modelAndView1 = new ModelAndView("error");
		return modelAndView1;

	}

	@RequestMapping("/BCHome")
	public ModelAndView homePage() {
		ModelAndView mView = new ModelAndView("BranchEmployee");
		return mView;
	}

}