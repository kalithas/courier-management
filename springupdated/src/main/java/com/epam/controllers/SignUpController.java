package com.epam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.epam.service.CustomerService;
import com.epam.service.LoginService;

@RestController
@RequestMapping("/SUC")
public class SignUpController {
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	CustomerService customerService;
	
	@PostMapping("/signup")
	public ModelAndView createCustomer(@RequestParam("userName") String userName, @RequestParam("email") String email, @RequestParam("password") String password, @RequestParam("repassword") String repassword) {
			boolean result = loginService.saveUser(email, password, userName);
			if(result == true) {
				ModelAndView mView = new ModelAndView("redirect:/signin/login");
				return mView;
			}
		ModelAndView mView1 = new ModelAndView("error");
		return mView1;
	}
	

}
