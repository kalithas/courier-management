package com.epam.controllers;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.epam.entity.Consignment;
import com.epam.repo.AirHubRepo;
import com.epam.service.AirHubService;
import com.epam.service.LoginService;
import com.epam.util.CookieUtil;
import com.epam.vo.ConsignmentVO;
import com.epam.vo.EditVO;

@RestController
@RequestMapping("/AHC")
public class AirHubController {
	private static final Logger logger = LogManager.getLogger(AirHubController.class);
	
	private int airHubId;
	private int airHubUserId;
	
	
	@Autowired
	AirHubService airHubService;
	
	@Autowired
	LoginService loginService;
	
	
	@Autowired
	AirHubRepo airHubRepo;
	
	@Autowired
	CookieUtil cookieUtil;



	@GetMapping("/incoming")
	public ModelAndView createConsignment() {
		
		airHubUserId = cookieUtil.getUserId();
		airHubId = airHubRepo.findByUserId(airHubUserId).getId();
		List<ConsignmentVO> resConsignments = airHubService.getIncomingConsignmentList(airHubId);
		 logger.info("airhub"+resConsignments.size());
		 logger.info("gonna return");
		ModelAndView mav = new ModelAndView("AirhubIncoming");
		mav.addObject("conVO", resConsignments);
		return mav;
	}


	@RequestMapping("/outgoing")
	public ModelAndView ListOfOutgoingconsignmnet() {
		
		airHubUserId = cookieUtil.getUserId();
		airHubId = airHubRepo.findByUserId(airHubUserId).getId();
		
		logger.info("Inside controller:ListOfOutgoingconsignmnet at airhub");
		List<Consignment> res = airHubService.ListOfOutgoingConsignment(airHubId);
		 logger.info(res.size());
		 logger.info("Airhub outgoing");
		ModelAndView mav = new ModelAndView("AirHubOutgoing");
		mav.addObject("conVO", res);
		logger.info("Leaving controller:ListOfOutgoingconsignmnet at airhub");
		return mav;
	}
	
	@GetMapping("/receive/{trackingId}")
	public ModelAndView outgoing(@PathVariable String trackingId) {
		 logger.info("Receive consignmnets going...................");
		 logger.info("TrackingId "+trackingId);
		ModelAndView mv = new ModelAndView("redirect:/AHC/incoming");
		airHubService.receiveConsignment(trackingId, airHubId);
		return mv;

	}
	
	///AHC/outgoing
	@GetMapping("/outgoing/{trackingId}")
	public ModelAndView outgoingConsignments(@PathVariable String trackingId) {
		airHubUserId = cookieUtil.getUserId();
		airHubId = airHubRepo.findByUserId(airHubUserId).getId();
		 logger.info("Receive consignmnets going...................");
		 logger.info("TrackingId "+trackingId);
		ModelAndView mv = new ModelAndView("redirect:/AHC/outgoing");
		airHubService.sendConsignment(trackingId, airHubId);
		return mv;

	}
	
	
	
	@RequestMapping("/AHCHome")
	public ModelAndView homePage() {
		ModelAndView mView = new ModelAndView("AirHubPage");
		return mView;
	}
	
	@GetMapping("/editProfile")
	public ModelAndView editProfile() {
		ModelAndView mView = new ModelAndView("EditAirHubProfile");

		return mView;
	}

	@PostMapping("/editProfile")
	public ModelAndView editProfile1(EditVO editVO) {
		String email = editVO.getEmail();
		String oldPassword = editVO.getOldPassword();
		String newPassword = editVO.getNewPassword();
		if (oldPassword != null && newPassword != null && loginService.updatePassword(email, oldPassword, newPassword)) {
			System.out.println("Updated succesfully...");
			ModelAndView mView = new ModelAndView("redirect:/CC/editProfile");
			return mView;
		} else {
			System.out.println("Wrong password...");
		}
		ModelAndView mView = new ModelAndView("error");
		return mView;
	}

}
