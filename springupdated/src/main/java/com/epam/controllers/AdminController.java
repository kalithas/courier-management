package com.epam.controllers;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.epam.entity.Branch;
import com.epam.entity.City;
import com.epam.entity.Feedback;
import com.epam.service.AdminService;
import com.epam.service.CityService;
import com.epam.service.LoginService;
import com.epam.util.CookieUtil;
import com.epam.util.CourierUtil;
import com.epam.vo.AirHubVO;
import com.epam.vo.BranchVO;
import com.epam.vo.SortingAgentVO;
import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/admin")
public class AdminController {
	private static final Logger logger = LogManager.getLogger(AdminController.class);


	private int adminUserId;
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	CookieUtil cookieUtil;

	@Autowired
	CityService cityService;

	@Autowired
	LoginService loginService;


	@GetMapping("/ModifyBranch")
	public ModelAndView getBranches3() {

		ModelAndView mView = new ModelAndView("ModifyBranch");

		List<City> cityList = cityService.getCities();
		List<BranchVO> bVO = new ArrayList<BranchVO>();
		for (City c : cityList) {
			if (c.getBranch().size() > 0) {
				for (Branch b : c.getBranch()) {
					BranchVO branchVO = new BranchVO();
					branchVO.setCity(c.getCityName());
					branchVO.setAddress(b.getAddress());
					branchVO.setZipCode(String.valueOf(b.getZipCode()));
					bVO.add(branchVO);
				}

			}
		}
		// logger.info(bVO);
		mView.addObject("branch", bVO);
		return mView;
	}

	@PostMapping("/ModifyBranch")
	public ModelAndView modifyBranch(Model m, @RequestParam("branchDetails") String branch,
			@RequestParam String address, @RequestParam("zipcode") String zipCode,
			@RequestParam("isactive") String isActive) {

		ModelAndView mav = new ModelAndView("redirect:/admin/branches");
		String[] zipCodes = branch.split("-");
		logger.info(zipCodes);
		String branchName = zipCodes[1];

		String zipCodeFirst = zipCodes[2];
		logger.info(branchName + " " + zipCodeFirst + " " + address + " " + zipCode + " " + isActive);
		if (adminService.updateBranch(branchName, zipCodeFirst, address, zipCode, isActive)) {
			return mav;
		}
		ModelAndView mav1 = new ModelAndView("Error");
		return mav1;
	}

	@RequestMapping(value = "/getAllFeedbacks", method = RequestMethod.GET)
	public ModelAndView getAllFeedbacks(Model m) {
		ModelAndView mv = new ModelAndView("ViewFeedback");
		List<Feedback> fList = adminService.getAllFeedbacks();

		mv.addObject("fList", fList);
		return mv;
	}

	@GetMapping("/addBranch")
	public ModelAndView getBranches(@ModelAttribute("city") City city) {

		ModelAndView mView = new ModelAndView("AddBranch");
		List<City> citys = cityService.getCities();
		// logger.info(citys);
		mView.addObject("city", citys);
		return mView;
	}

	@PostMapping("/addBranch")
	public ModelAndView addBranch(Model m, BranchVO b) {
		logger.info("Entered: " + b);
		String password = CourierUtil.getPassword();
		String cityName = b.getCity();
		String[] str = cityName.split(",");
		for (int i = 0; i < str.length; i++) {
			logger.info(str[i].trim());
		}
		if (str.length == 2) {
			cityName = str[1];
		} else {
			cityName = str[0];
		}
	    adminUserId = cookieUtil.getUserId();
		int id = adminService.addBranch(adminUserId, b.getMail(), password, b.getAddress(), b.getZipCode(), cityName);
		logger.info("Branch id: " + id);
		ModelAndView mView = new ModelAndView("redirect:/admin/branches");
		return mView;
	}

	@GetMapping("/addSortingHub")
	public ModelAndView addSortingHub() {
		ModelAndView mAndView = new ModelAndView("AddSortingHub");
		List<City> citys = cityService.getCities();
		// logger.info(citys);
		mAndView.addObject("city", citys);
		return mAndView;
	}

	@PostMapping("/addSortingHub")
	public ModelAndView addSortingAgent(Model m, SortingAgentVO s) {
		logger.info(s.getCity());
		String cityName = s.getCity();
		String[] str = cityName.split(",");
		for (int i = 0; i < str.length; i++) {
			logger.info(str[i].trim());
		}
		if (str.length == 2) {
			cityName = str[1];
		} else {
			cityName = str[0];
		}
		String password = CourierUtil.getPassword();
		adminUserId = cookieUtil.getUserId();
		int id = adminService.addSortingAgent(adminUserId, s.getMail(), password, cityName);
		m.addAttribute("str", "Sortinghub added");
		m.addAttribute("id", id);
		ModelAndView mView = new ModelAndView("redirect:/admin/addSortingHub");
		return mView;
	}

	@GetMapping("/addAirHub")
	public ModelAndView addAirHub() {
		ModelAndView mAndView = new ModelAndView("AddAirHub");
		List<City> citys = cityService.getCities();
		// logger.info(citys);
		mAndView.addObject("city", citys);
		return mAndView;
	}

	@PostMapping("/addAirHub")
	public ModelAndView addAirHub(Model m, AirHubVO a) {
		String cityName = a.getCity();
		String[] str = cityName.split(",");
		for (int i = 0; i < str.length; i++) {
			logger.info(str[i].trim());
		}
		if (str.length == 2) {
			cityName = str[1];
		} else {
			cityName = str[0];
		}
		String password = CourierUtil.getPassword();
		adminUserId = cookieUtil.getUserId();
		int id = adminService.addAirHub(adminUserId, a.getMail(), password, cityName);
		m.addAttribute("str", "Sortinghub added");
		m.addAttribute("id", id);
		ModelAndView mView = new ModelAndView("redirect:/admin/addAirHub");
		return mView;
	}

	@GetMapping("/adminHome")
	public ModelAndView adminHome() {

		ModelAndView mView = new ModelAndView("AdminHome");

		return mView;
	}

	@GetMapping("/report")
	public ModelAndView getReports(@ModelAttribute("branch") Branch branch) {

		ModelAndView mView = new ModelAndView("ViewReports");
		List<City> cityList = cityService.getCities();
		// logger.info(cityList);
		List<BranchVO> bVO = new ArrayList<BranchVO>();
		for (City c : cityList) {
			if (c.getBranch().size() > 0) {
				for (Branch b : c.getBranch()) {
					BranchVO branchVO = new BranchVO();
					branchVO.setCity(c.getCityName());
					branchVO.setAddress(b.getAddress());
					branchVO.setZipCode(String.valueOf(b.getZipCode()));
					bVO.add(branchVO);
				}

			}
		}

		mView.addObject("branch", bVO);
		return mView;
	}

	@PostMapping("/report")
	public ModelAndView getMonthConsignments(@RequestParam("month") String month, @RequestParam("year") String year,
			@RequestParam("branchdetails") String city) throws FileNotFoundException, JRException {
		String[] branchZipcode = city.split("-");
		ModelAndView model = new ModelAndView("ViewReportData");
		model.addObject("report", adminService.generateReport(Integer.parseInt(month), Integer.parseInt(year),
				Integer.parseInt(branchZipcode[1])));
		return model;
	}

	@RequestMapping("/branches")
	public ModelAndView getBranches4() {

		ModelAndView mView = new ModelAndView("ViewBranches");

		List<City> cityList = cityService.getCities();
		// logger.info(cityList);
		List<BranchVO> bVO = new ArrayList<BranchVO>();
		for (City c : cityList) {
			if (c.getBranch().size() > 0) {
				for (Branch b : c.getBranch()) {
					BranchVO branchVO = new BranchVO();
					branchVO.setCity(c.getCityName());
					branchVO.setAddress(b.getAddress());
					branchVO.setZipCode(String.valueOf(b.getZipCode()));
					bVO.add(branchVO);
				}

			}
		}

		mView.addObject("branch", bVO);
		return mView;
	}

}
