package com.epam.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.entity.Branch;
import com.epam.entity.City;
import com.epam.service.AdminService;
import com.epam.service.CityService;
import com.epam.service.ConsignmentService;
import com.epam.service.UserService;
import com.epam.util.CourierUtil;
import com.epam.vo.BranchVO;
import com.epam.vo.ConsignmentVO;
import com.epam.vo.UserVO;

@Controller
@RequestMapping("/OC")
public class OpenController {
	private static final Logger logger = LogManager.getLogger(OpenController.class);
	@Autowired
	CityService cityService;

	@Autowired
	AdminService adminService;

	@Autowired
	UserService userService;

	@Autowired
	ConsignmentService consignmentService;

	@GetMapping("/branch")
	public ModelAndView searchBranch() {
		ModelAndView mView = new ModelAndView("Branch");
		return mView;
	}

	@PostMapping("/branch")
	public ModelAndView displayBranches(@RequestParam("city") String city) {
		ModelAndView mView = new ModelAndView("ViewBranchesHome");
		logger.info("city name" + city);
		List<City> cityList = cityService.getCities();
		logger.info("city size" + cityList.size());
		List<BranchVO> bVO = new ArrayList<BranchVO>();
		for (City c : cityList) {
			if (c.getCityName().equalsIgnoreCase(city)) {
				logger.info(" inside c.getCityName()==city");
				if (c.getBranch().size() > 0) {
					logger.info(" inside c.getBranch()>0");
					for (Branch b : c.getBranch()) {

						logger.info(" inside get branch");
						BranchVO branchVO = new BranchVO();
						branchVO.setCity(c.getCityName());
						branchVO.setAddress(b.getAddress());
						branchVO.setZipCode(String.valueOf(b.getZipCode()));
						bVO.add(branchVO);
					}

				}
			}
		}
		if (bVO.size() == 0) {
			logger.info("size" + bVO.size());
			logger.info("else part");
			ModelAndView mV = new ModelAndView("NoBranches");
			mV.addObject("city", city);
			return mV;
		}

		mView.addObject("city", city);
		logger.info(bVO.size());
		mView.addObject("branch", bVO);
		return mView;

	}

	@GetMapping("/services")
	public ModelAndView viewServices() {
		ModelAndView mView = new ModelAndView("Services");
		return mView;
	}

	@GetMapping("/aboutUs")
	public ModelAndView viewAboutUs() {
		ModelAndView mView = new ModelAndView("About");
		return mView;
	}

	@GetMapping("/contact")
	public ModelAndView viewContact() {
		ModelAndView mView = new ModelAndView("Contact");
		return mView;
	}

	@PostMapping("/trackConsignmentPublic")
	public ModelAndView trackConsignment1(@RequestParam("trackId") String trackId) {
		logger.info("calling...");
		ConsignmentVO cvo = consignmentService.trackConsignment(trackId);
		System.out.println("enetred controller");
		ModelAndView mView = new ModelAndView("ConsignmentStatus");
		mView.addObject("st", cvo);
		List<ConsignmentVO> reStatusTrackingDetails = consignmentService.trackConsignmentAllStatus(trackId);
		System.out.println("size of tracking details" + reStatusTrackingDetails.size());
		mView.addObject("allStatusReport", reStatusTrackingDetails);
		return mView;
	}

	@GetMapping("/fp")
	public String forgot(Model m) {
		return "forget-page";
	}

	@PostMapping("/finalpage")
	public ModelAndView finalpage(Model m, UserVO userVo) {
		logger.info("Inside finalPage method");
		logger.info(userVo);
		int userId = userService.getUserId(userVo.getMail());
		logger.info(userId);
		if (CourierUtil.verifyOtp(userId, userVo.getOtp())) {
			logger.info("verified 123");
			ModelAndView mv = new ModelAndView("change-password");
			mv.addObject("userVo", userVo);
			return mv;
		} else {
			m.addAttribute("msg", "Incorrect otp");
		}
		logger.info("Reached untill here, really good work");
		return null;
	}

	@PostMapping("/success")
	public String success(@RequestParam String password1, @RequestParam("mail1") String mail1) {
//		 logger.info("Mail from change password page1 : "+mail);
		logger.info("Mail from change password page2 : " + mail1);
		userService.changePassword(password1, mail1);
		logger.info("Inside the login");
		return "Login";
	}
	
	@GetMapping("/parcelDelivery")
	public String parcelDelivery(Model m) {
		return "ParcelDelivery";
	}

}
