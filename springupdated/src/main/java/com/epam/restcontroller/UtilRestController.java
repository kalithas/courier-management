package com.epam.restcontroller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.epam.service.UserService;
import com.epam.vo.UserVO;

@RestController
public class UtilRestController {
	private static final Logger logger = LogManager.getLogger(UtilRestController.class);
	@Autowired
	UserService userService;

	@PutMapping("/user")
	public ResponseEntity<String> addUser(@RequestBody UserVO userVo) {
		 logger.info("otp received here by me");
		String mail = userVo.getMail();
		 logger.info("otp received here by me");
		 logger.info("mail " + mail);
		userService.sendMail(mail);

		return new ResponseEntity<>("Otp sent successfully", HttpStatus.OK);
	}


}