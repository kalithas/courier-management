package com.epam.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.epam.entity.Admin;


@Repository
public interface AdminRepo extends JpaRepository<Admin, Integer>{
	@Query(value = "select * from admin where user_id = :id", nativeQuery = true)
	public List<Admin> getAdminDetails(@Param("id") int id);
}
