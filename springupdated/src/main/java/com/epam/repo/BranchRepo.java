package com.epam.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.entity.Branch;

@Repository
public interface BranchRepo extends JpaRepository<Branch, Integer> {
	@Query("SELECT b FROM Branch b")
	public List<Branch> getBranches();

	@Query("SELECT b FROM Branch b WHERE b.zipCode = :zipCodeFirst AND b.address = :address")
	public Branch getId(@Param("zipCodeFirst") String zipCodeFirst, @Param("address") String branchName);

	@Query("SELECT b.id FROM Branch b WHERE b.zipCode = :zipCode")
	public int findByZipcode(@Param("zipCode") int zipCode);

	@Query("SELECT b FROM Branch b INNER JOIN b.user u WHERE u.id = :id")
	public List<Branch> getBranchDetails(@Param("id") int id);


	@Query(value = "SELECT concat(b.id,',',c.tracking_id,',',c.source,',',c.destination,',',c.created_at) as report FROM t_consignment c INNER JOIN t_branch b ON b.id=c.branch_id WHERE b.user_id=:branchId AND MONTH(c.created_at)= :month AND YEAR(c.created_at)=:year", nativeQuery = true)
	public List<String> getMonthlyBranchConsignments(@Param("month") int month, @Param("year") int year,
			@Param("branchId") int branchId);
	
//	@Query(value = "SELECT concat(b.id,',',c.tracking_id,',',c.source,',',c.destination,',',c.created_at) as report FROM t_consignment c INNER JOIN t_branch b ON b.id=c.branch_id WHERE b.user_id=:branchId", nativeQuery = true)
//	public List<String> getMonthlyBranchConsignments(
//			@Param("branchId") int branchId);

	public List<Branch> findBranchByCityId(int id);
	public Branch findByUserId(int id);

}