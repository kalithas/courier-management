package com.epam.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.epam.entity.DeliveryPerson;

@Repository
public interface DeliveryPersonRepo extends JpaRepository<DeliveryPerson, Integer> {

	public DeliveryPerson findByUserId(int userId);
	public DeliveryPerson findById(int dId);

	@Query("SELECT d FROM DeliveryPerson d INNER JOIN d.user u WHERE u.id = :id")
	public List<DeliveryPerson> getDeliveryPersonDetails(@Param("id") int id);

	@Query("SELECT d FROM DeliveryPerson d INNER JOIN d.sortingAgent s WHERE s.id = :id")
	public List<DeliveryPerson> findAllDeliveryPersonBySortingAgentId(@Param("id") int id);


}