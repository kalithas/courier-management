package com.epam.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.entity.Feedback;

@Repository
public interface FeedbackRepo extends JpaRepository<Feedback, Integer> {
	@Query("SELECT f FROM Feedback f INNER JOIN f.consignment c INNER JOIN f.customer cust WHERE c.id = :consignId AND cust.id = :custId")
	public Feedback findByCustomerAndConsignemntId(@Param("custId") int id, @Param("consignId") int cid);
}