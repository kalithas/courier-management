package com.epam.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.epam.entity.Role;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {
	@Transactional
	@Query("SELECT r.id FROM Role r WHERE r.code = :role")
	public int getRole(@Param("role") String role);
	public Role findByCode(String code);

}