package com.epam.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.epam.entity.City;

@Repository
public interface CityRepo extends JpaRepository<City, Integer> {
	public City findByCityName(String destination);

	@Transactional
	@Query("SELECT c.id FROM City c WHERE c.cityName = :name")
	public int getIDofCity(@Param("name") String name);

}