package com.epam.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.entity.Consignment;
import com.epam.entity.Customer;

@Repository
public interface ConsignmentRepo extends JpaRepository<Consignment, Integer> {

	@Query(value = "SELECT * FROM t_consignment c INNER JOIN t_branch b ON b.id=c.branch_id WHERE b.zipcode=:zipcode AND MONTH(c.created_at)= :month AND YEAR(c.created_at)=:year", nativeQuery = true)
	List<Consignment> getMonthlyCityConsignments(@Param("month") int month, @Param("year") int year,
			@Param("zipcode") int zipcode);

	@Query(value = "SELECT c.name,b.zipcode as branchcode FROM t_city c INNER JOIN t_branch b "
			+ " ON c.id=b.city_id", nativeQuery = true)
	List<String> getAllCityWithZipcode();

	public Consignment findByTrackingId(String trackId);

	@Query("SELECT c FROM Consignment c INNER JOIN c.deliveryPerson d WHERE d.id = :id")
	List<Consignment> findByDeliveryPerson(int id);

	@Query("SELECT c FROM Consignment c INNER JOIN c.sortingAgent s WHERE s.id = :saID")
	public List<Consignment> findConsignmentsByCity(@Param("saID") int sortinAgentId);

	Consignment findByStatusId(int id);

	@Query(value = "SELECT c.tracking_id FROM t_consignment c, t_user u where u.email=:emailId ORDER BY c.id DESC LIMIT 1", nativeQuery = true)
	public String findTrackingId(@Param("emailId") String emailId);

	public Consignment findById(int id);

	@Query("SELECT c FROM Consignment c INNER JOIN c.sortingAgent s WHERE s.id = :id")
	public List<Consignment> getAllOutgoingConsignments(@Param("id") int id);

	@Query("SELECT c FROM Consignment c INNER JOIN c.status s WHERE s.id = :id")
	public Consignment findByStatus(@Param("id") int sId);

	@Query("SELECT c.destinationZipcode FROM Consignment c INNER JOIN c.deliveryPerson d INNER JOIN c.status s WHERE d.id = :id AND s.id = :statusId")
	public Integer findIfAssigned(@Param("id") int id, @Param("statusId") int statusId);

	@Query("SELECT s.id FROM Consignment c INNER JOIN c.status s WHERE c.id = :id")
	public int findStatusByConsignmentId(@Param("id") int id);

	Consignment findByCustomer(Customer customer);

}