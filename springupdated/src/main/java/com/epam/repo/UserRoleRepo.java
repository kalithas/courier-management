package com.epam.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.entity.UserRole;

@Repository
public interface UserRoleRepo extends JpaRepository<UserRole, Integer> {

	@Query("SELECT ur FROM UserRole ur INNER JOIN ur.user u WHERE u.id = :id")
	public List<UserRole> checkRole(@Param("id") int id);

	@Query("SELECT r.id FROM UserRole ur INNER JOIN ur.user u INNER JOIN ur.role r WHERE u.id = :id")
	Integer findRoleIdByUserId(@Param("id") int id);

}