package com.epam.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.entity.IOStatus;
import com.epam.entity.Status;

@Repository
public interface StatusRepo extends JpaRepository<Status, Integer> {
	public Status findById(int id);
	public List<Status> findByCodeAndIoStatus(int id, IOStatus code);
	public Status findIdByCodeAndIoStatus(int id, IOStatus iostatus);
	@Query("SELECT s.id FROM Status s WHERE s.code = :id")
	public Integer findStatusByCode(@Param("id") int id);
}