package com.epam.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.epam.entity.AirHub;
import com.epam.entity.City;

@Repository
public interface AirHubRepo extends JpaRepository<AirHub, Integer>{
	public AirHub findById(int id);
	public AirHub findByCity(City city);
	public AirHub findByUserId(int id);
}
