package com.epam.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.epam.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>{
	public User findByEmail(String email);
	@Query("select u from User u where u.id=:id")
	public User findId(@Param("id")int id) ;
	public User findById(int id);
	

}
