package com.epam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.entity.Cookie;

public interface CookieRepo extends JpaRepository<Cookie, Integer>{
	Cookie findById(int id);
}
