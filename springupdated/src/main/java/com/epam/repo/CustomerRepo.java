package com.epam.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.epam.entity.Customer;
import com.epam.entity.User;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {

	@Query("SELECT c FROM Customer c INNER JOIN c.user u WHERE u.id = :id")
	public List<Customer> getCustomerDetails(@Param("id") int id);
	Customer findByUserId(int userId);
	Customer findByUser(User user);
	Customer findById(int customerId);

}