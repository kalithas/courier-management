package com.epam.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.entity.City;
import com.epam.entity.SortingAgent;

@Repository
public interface SortingAgentRepo extends JpaRepository<SortingAgent, Integer>{
	public SortingAgent findById(int id);
	public SortingAgent findByCity(City city);
	public SortingAgent findByUserId(int userId);
	
}
