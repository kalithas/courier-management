package com.epam.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.entity.User;

@Repository
public interface LoginRepo extends JpaRepository<User, Integer>{
	@Query("SELECT u FROM User u WHERE u.email = :email AND u.password = :password")
	public List<User> checkLogin(@Param("email") String email, @Param("password") String password);
}
