package com.epam.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.entity.StatusTrackingDetails;

@Repository
public interface StatusTrackingDetailsRepo extends JpaRepository<StatusTrackingDetails, Integer> {
	@Query("SELECT std FROM StatusTrackingDetails std WHERE std.statusIdFinal = :id")
	public List<StatusTrackingDetails> getByStatusId(@Param("id") int id);

}